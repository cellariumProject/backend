# Import library
from pydantic import BaseModel
from typing import List, Optional
from app.database import init_models


# Init Database Models for relation generation
init_models()


class Token(BaseModel):
    """
    Token Model Definition
    """
    access_token: str
    token_type: str


class TokenData(BaseModel):
    """
    Token User Data Model Definition
    """
    user_id: Optional[str] = None
    scopes: List[str] = []
