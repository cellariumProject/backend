# Import library
from tortoise import fields, models


class ShoppingList(models.Model):
    """
    Shopping List Database Definition
    """

    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=250, null=False)
    user = fields.ForeignKeyField("models.User", related_name="shopping_lists")
