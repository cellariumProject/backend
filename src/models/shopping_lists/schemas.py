# Import library
from tortoise.contrib.pydantic import pydantic_model_creator

from app.database import init_models

from models.shopping_lists import models


# Init Database Models for relation generation
init_models()


ShoppingListIn = pydantic_model_creator(models.ShoppingList, name="ShoppingListIn", exclude_readonly=True)
ShoppingListOut = pydantic_model_creator(models.ShoppingList, name="ShoppingListOut")
