# Import library
from tortoise import fields, models
from enum import Enum


class SortTypeEnum(str, Enum):
    ASC = "ASC"
    DESC = "DESC"


class SortEnum(str, Enum):
    id = "id"
    name = "name"
    img_path = "img_path"
    default_quantity = "default_quantity"
    brand__name = "brand__name"
    unit__name = "unit__name"
    section__name = "section__name"
    category__name = "category__name"


class Item(models.Model):
    """
    Items Database Definition
    """

    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=250, null=False)
    img_path = fields.CharField(max_length=350, null=False)
    default_quantity = fields.FloatField(null=True)
    brand = fields.ForeignKeyField("models.Brand", related_name="items")
    unit = fields.ForeignKeyField("models.Unit", related_name="items")
    section = fields.ForeignKeyField("models.Section", related_name="items")
    category = fields.ForeignKeyField("models.Category", related_name="items")

    def brand_id(self) -> int:
        return self.brand.id

    def unit_id(self) -> int:
        return self.unit.id

    def section_id(self) -> int:
        return self.section.id

    def category_id(self) -> int:
        return self.category.id

    class PydanticMeta:
        exclude = ["user_items"]
        computed = ["brand_id", "unit_id", "section_id", "category_id"]
