# Import library
from tortoise.functions import Count
from tortoise.query_utils import Q
from datetime import date, timedelta, datetime

from models.stats import schemas

from models.users import models as usersModels
from models.user_items import models as userItemsModels


MAX_DAYS = 120


async def get_in_out_count(
        user: usersModels.User,
        date_min: date,
        date_max: date):
    """
    Get Input Output count.
    """
    in_count = await get_in_count(user=user, date_min=date_min, date_max=date_max)
    out_count = await get_out_count(user=user, date_min=date_min, date_max=date_max)

    db_data_dict = {}

    for element in in_count:
        if element in db_data_dict:
            db_data_dict[element]['in_count'] += in_count[element]['in_count']
        else:
            db_data_dict[element] = {
                'in_count': in_count[element]['in_count'],
                'out_count': 0,
                'stock_count': await get_stock_count(
                    user=user,
                    date_count=element
                )
            }

    for element in out_count:
        if element in db_data_dict:
            db_data_dict[element]['out_count'] += out_count[element]['out_count']
        else:
            db_data_dict[element] = {
                'in_count': 0,
                'out_count': out_count[element]['out_count'],
                'stock_count': await get_stock_count(
                    user=user,
                    date_count=element
                )
            }

    date_tmp = date_min
    i = 0
    while date_tmp <= date_max and i < MAX_DAYS:
        if date_tmp not in db_data_dict:
            db_data_dict[date_tmp] = {
                'in_count': 0,
                'out_count': 0,
                'stock_count': await get_stock_count(
                    user=user,
                    date_count=date_tmp
                )
            }
        date_tmp = date_tmp + timedelta(days=1)
        i += 1

    db_data = []

    for element in sorted(db_data_dict):
        db_data.append(
            schemas.UserItemIO(
                io_date=element,
                in_count=db_data_dict[element]['in_count'],
                out_count=db_data_dict[element]['out_count'],
                stock_count=db_data_dict[element]['stock_count']
            )
        )

    return db_data


async def get_in_count(
        user: usersModels.User,
        date_min: date,
        date_max: date):
    """
    Get Input count between date.
    """
    tmp_in_count = await userItemsModels.UserItem.filter(
        Q(user=user),
        Q(created_at__range=(date_min, date_max))
    ).annotate(
        in_count=Count("id")
    ).group_by(
        "created_at"
    ).values("created_at", "in_count")

    in_count = {}
    for element in tmp_in_count:
        if element["created_at"].date() in in_count:
            in_count[element["created_at"].date()]["in_count"] += element["in_count"]
        else:
            in_count[element["created_at"].date()] = {"in_count": element["in_count"]}

    return in_count


async def get_out_count(
        user: usersModels.User,
        date_min: date,
        date_max: date):
    """
    Get Output count between date.
    """
    tmp_out_count = await userItemsModels.UserItem.filter(
        Q(user=user),
        Q(deleted_at__range=(date_min, date_max)),
        Q(deleted_at__not=date(year=1970, month=1, day=1)),
        Q(deleted_at__not_isnull=True)
    ).annotate(
        out_count=Count("id")
    ).group_by(
        "deleted_at"
    ).values("deleted_at", "out_count")

    out_count = {}
    for element in tmp_out_count:
        if element["deleted_at"] in out_count:
            out_count[element["deleted_at"]]["out_count"] += element["out_count"]
        else:
            out_count[element["deleted_at"]] = {"out_count": element["out_count"]}

    return out_count


async def get_stock_count(
        user: usersModels.User,
        date_count: date):
    """
    Get Stock count of date.
    """
    date_count = datetime(
        year=date_count.year,
        month=date_count.month,
        day=date_count.day,
        hour=23,
        minute=59,
        second=59
    )
    return await userItemsModels.UserItem.filter(
        Q(user=user),
        Q(created_at__lte=date_count),
        Q(Q(deleted_at__gt=date_count) | Q(
            deleted_at=date(year=1970, month=1, day=1)) | Q(
                deleted_at__isnull=True))
    ).count()


async def get_waste_count_between_date(
        user: usersModels.User,
        date_min: date,
        date_max: date):
    """
    Get Waste count between date.
    """
    return await userItemsModels.UserItem.filter(
        Q(user=user),
        Q(deleted_at__range=(date_min, date_max)),
        Q(wasted=True),
        Q(deleted_at__not=date(year=1970, month=1, day=1)),
        Q(deleted_at__not_isnull=True)
    ).annotate(
        wasted_count=Count("wasted")
    ).group_by(
        "deleted_at"
    ).values("deleted_at", "wasted_count")
