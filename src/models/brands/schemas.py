# Import library
from tortoise.contrib.pydantic import pydantic_model_creator
from pydantic.main import BaseModel

from app.database import init_models

from models.brands import models


# Init Database Models for relation generation
init_models()

BrandIn = pydantic_model_creator(models.Brand, name="BrandIn", exclude_readonly=True)
BrandOut = pydantic_model_creator(models.Brand, name="BrandOut")


class Filters(BaseModel):
    skip: int = 0
    limit: int = None
    search: str = None
    order_by: models.SortEnum = models.SortEnum.name
    order_by_type: models.SortTypeEnum = models.SortTypeEnum.ASC
