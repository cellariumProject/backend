# Import library
from tortoise.query_utils import Q
from faker import Faker

from models.units import models
from models.units import schemas


async def get_all(filters: schemas.Filters = schemas.Filters()):
    """
    Get All Units.
    """
    filter_args = []

    if filters.search is not None:
        filter_args.append(Q(name__icontains=filters.search))

    if filters.order_by is not None and filters.order_by_type is not None:
        if filters.order_by_type == models.SortTypeEnum.ASC:
            order_by_args = filters.order_by.value
        else:
            order_by_args = "-{}".format(filters.order_by.value)
    else:
        order_by_args = models.SortEnum.name

    db_data = []

    if filters.limit is None:
        db_data = await models.Unit.filter(*filter_args).order_by(
            order_by_args).offset(filters.skip)
    else:
        db_data = await models.Unit.filter(*filter_args).order_by(
            order_by_args).offset(filters.skip).limit(filters.limit)

    return db_data


async def get_nb(filters: schemas.Filters = schemas.Filters()):
    """
    Get Nb Units.
    """
    filter_args = []

    if filters.search is not None:
        filter_args.append(Q(name__icontains=filters.search))

    return await models.Unit.filter(*filter_args).count()


async def get(id: int):
    """
    Get Unit by ID.
    """
    return await models.Unit.get_or_none(id=id)


async def get_by_name(name: str):
    """
    Get Unit by name.
    """
    return await models.Unit.get_or_none(name=name)


async def create(unit: schemas.UnitIn):
    """
    Create Unit.
    """
    return await models.Unit.create(**unit.dict(exclude_unset=True))


async def update(unit: schemas.UnitIn, id: int):
    """
    Update Unit.
    """
    await models.Unit.get_or_none(id=id).update(**unit.dict(exclude_unset=True))
    return await get(id=id)


async def delete(id: int):
    """
    Delete Unit.
    """
    return await models.Unit.get_or_none(id=id).delete()


async def faker(nb: int):
    """
    Generate fake data
    """
    faker = Faker()
    db_data = []
    for i in range(nb):
        db_data.append(await create(unit=schemas.UnitIn(
            name=faker.word()
        )))
    return db_data
