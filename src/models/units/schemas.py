# Import library
from tortoise.contrib.pydantic import pydantic_model_creator
from pydantic.main import BaseModel

from app.database import init_models

from models.units import models


# Init Database Models for relation generation
init_models()

UnitIn = pydantic_model_creator(models.Unit, name="UnitIn", exclude_readonly=True)
UnitOut = pydantic_model_creator(models.Unit, name="UnitOut")


class Filters(BaseModel):
    skip: int = 0
    limit: int = None
    search: str = None
    order_by: models.SortEnum = models.SortEnum.name
    order_by_type: models.SortTypeEnum = models.SortTypeEnum.ASC
