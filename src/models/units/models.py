# Import library
from tortoise import fields, models
from enum import Enum


class SortTypeEnum(str, Enum):
    ASC = "ASC"
    DESC = "DESC"


class SortEnum(str, Enum):
    id = "id"
    name = "name"


class Unit(models.Model):
    """
    Unit Database Definition
    """

    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=250, null=False, unique=True)

    class PydanticMeta:
        exclude = ["items"]
