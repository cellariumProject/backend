# Import library
from tortoise.contrib.pydantic import pydantic_model_creator
from pydantic.main import BaseModel

from app.database import init_models

from models.sections import models


# Init Database Models for relation generation
init_models()

SectionIn = pydantic_model_creator(models.Section, name="SectionIn", exclude_readonly=True)
SectionOutBase = pydantic_model_creator(models.Section, name="SectionOutBase")


class SectionOut(SectionOutBase):
    '''
    Section Model Definition
    '''
    class Config:
        schema_extra = {
            'example': {
                'name': 'Meat',
                'color': '#41AF7B',
            }
        }


class Filters(BaseModel):
    skip: int = 0
    limit: int = None
    search: str = None
    order_by: models.SortEnum = models.SortEnum.name
    order_by_type: models.SortTypeEnum = models.SortTypeEnum.ASC
