# Import library
from tortoise.query_utils import Q
from random import choice
from random import uniform

from models.shopping_items import models
from models.shopping_items import schemas

from models.users import models as usersModels
from models.items import crud as itemsCrud
from models.shopping_lists import crud as shoppingListsCrud
from models.brands import crud as brandCrud
from models.units import crud as unitCrud
from models.sections import crud as sectionCrud
from models.categories import crud as categoryCrud

from models.shopping_lists import models as shoppingListsModels


PREFETCH_RELATED = [
    "item__brand",
    "item__unit",
    "item__section",
    "item__category",
    "shopping_list__user"
]


async def get_all_by_user_by_shopping_list(
        user: usersModels.User,
        shopping_list: shoppingListsModels.ShoppingList,
        filters: schemas.ShoppingItemFilter = schemas.ShoppingItemFilter()):
    """
    Get All Shopping Items.
    """
    if filters.skip is None:
        filters.skip = 0

    filter_args = [Q(shopping_list__user=user, shopping_list=shopping_list)]

    if filters.search is not None:
        filter_args.append(Q(item__name__icontains=filters.search))

    if filters.brand_id_list is None:
        filters.brand_id_list = [item.id for item in await brandCrud.get_all()]
    filter_args.append(Q(item__brand__id__in=filters.brand_id_list))

    if filters.unit_id_list is None:
        filters.unit_id_list = [item.id for item in await unitCrud.get_all()]
    filter_args.append(Q(item__unit__id__in=filters.unit_id_list))

    if filters.section_id_list is None:
        filters.section_id_list = [item.id for item in await sectionCrud.get_all()]
    filter_args.append(Q(item__section__id__in=filters.section_id_list))

    if filters.category_id_list is None:
        filters.category_id_list = [item.id for item in await categoryCrud.get_all()]
    filter_args.append(Q(item__category__id__in=filters.category_id_list))

    if filters.check is not None:
        filter_args.append(Q(check=filters.check))

    order_by_args = []

    if filters.order_by is not None:
        for order_by in filters.order_by:
            if order_by.order == models.SortTypeEnum.ASC:
                order_by_args.append(await getSortEnum(order_by.name))
            else:
                order_by_args.append("-{}".format(await getSortEnum(order_by.name)))
    else:
        order_by_args = ["item__name"]

    shoppingItemsList = []

    # Get Shopping Items with filters
    if filters.limit is None:
        shoppingItemsList = await models.ShoppingItem.filter(*filter_args).order_by(
            *order_by_args
        ).prefetch_related(*PREFETCH_RELATED).offset(filters.skip)
    else:
        shoppingItemsList = await models.ShoppingItem.filter(*filter_args).order_by(
            *order_by_args
        ).prefetch_related(*PREFETCH_RELATED).offset(filters.skip).limit(filters.limit)

    nbTotalShoppingItems = 0

    # Get number of items with filters
    nbTotalShoppingItems = await models.ShoppingItem.filter(
        *filter_args).prefetch_related(*PREFETCH_RELATED).count()

    filter_list_tmp = await models.ShoppingItem.filter(*filter_args).order_by(
        *order_by_args
    ).prefetch_related(*PREFETCH_RELATED).values(
        "item__brand__id",
        "item__brand__name",
        "item__unit__id",
        "item__unit__name",
        "item__section__id",
        "item__section__name",
        "item__section__color",
        "item__category__id",
        "item__category__name",
        "item__category__color"
    )

    brandListAvailable = {}
    unitListAvailable = {}
    sectionListAvailable = {}
    categoryListAvailable = {}

    for filter in filter_list_tmp:
        if filter["item__brand__id"] not in brandListAvailable:
            brandListAvailable[filter["item__brand__id"]] = {"name": filter["item__brand__name"]}
        if filter["item__unit__id"] not in unitListAvailable:
            unitListAvailable[filter["item__unit__id"]] = {"name": filter["item__unit__name"]}
        if filter["item__section__id"] not in sectionListAvailable:
            sectionListAvailable[filter["item__section__id"]] = {
                "name": filter["item__section__name"],
                "color": filter["item__section__color"]
            }
        if filter["item__category__id"] not in categoryListAvailable:
            categoryListAvailable[filter["item__category__id"]] = {
                "name": filter["item__category__name"],
                "color": filter["item__category__color"]
            }

    return schemas.ShoppingItemOutFilter(
        shopping_items=shoppingItemsList,
        nb_total_shopping_item=nbTotalShoppingItems,
        brand_list_available=brandListAvailable,
        unit_list_available=unitListAvailable,
        section_list_available=sectionListAvailable,
        category_list_available=categoryListAvailable
    )


async def get_all_names_by_user_by_shopping_list(
        user: usersModels.User,
        shopping_list: shoppingListsModels.ShoppingList):
    """
    Get All Shopping Items names
    """
    return await models.ShoppingItem.all().order_by(
        "item__name").prefetch_related(
            *PREFETCH_RELATED).filter(
                shopping_list__user=user,
                shopping_list=shopping_list).distinct().values_list(
                    "item__name", flat=True)


async def get_by_user(id: int, user: usersModels.User):
    """
    Get Shopping Item by ID and by User.
    """
    return await models.ShoppingItem.get_or_none(id=id, shopping_list__user=user).prefetch_related(*PREFETCH_RELATED)


async def get(id: int):
    """
    Get Shopping Item by ID.
    """
    return await models.ShoppingItem.get_or_none(id=id).prefetch_related(*PREFETCH_RELATED)


async def create(shopping_item: schemas.ShoppingItemIn):
    """
    Create Shopping Item.
    """
    db_data = await models.ShoppingItem.create(**shopping_item.dict(exclude_unset=True))
    return await get(id=db_data.id)


async def update(shopping_item: schemas.ShoppingItemIn, id: int):
    """
    Update Shopping Item.
    """
    await models.ShoppingItem.get_or_none(id=id).update(**shopping_item.dict(exclude_unset=True))
    return await get(id=id)


async def delete(id: int):
    """
    Delete Shopping Item.
    """
    return await models.ShoppingItem.get_or_none(id=id).delete()


async def faker(nb: int, shopping_list_id: int):
    """
    Generate fake data
    """
    items = await itemsCrud.get_all()
    items = items.items
    shoppingList = await shoppingListsCrud.get(id=shopping_list_id)
    db_data = []
    for i in range(nb):
        db_data.append(await create(shopping_item=schemas.ShoppingItemIn(
            quantity=uniform(1, 10000),
            check=choice([True, False]),
            item_id=choice(items).id,
            shopping_list_id=shoppingList.id
        )))
    return db_data


async def getSortEnum(order_by_name):
    """
    Translate SortEnum
    """
    if order_by_name == "default_quantity":
        return "quantity"
    if order_by_name == "name":
        return "item__name"
    if order_by_name == "brand__name":
        return "item__brand__name"
    if order_by_name == "unit__name":
        return "item__unit__name"
    if order_by_name == "section__name":
        return "item__section__name"
    if order_by_name == "category__name":
        return "item__category__name"
