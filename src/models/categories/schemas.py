# Import library
from tortoise.contrib.pydantic import pydantic_model_creator
from pydantic.main import BaseModel

from app.database import init_models

from models.categories import models


# Init Database Models for relation generation
init_models()

CategoryIn = pydantic_model_creator(models.Category, name="CategoryIn", exclude_readonly=True)
CategoryOutBase = pydantic_model_creator(models.Category, name="CategoryOutBase")


class CategoryOut(CategoryOutBase):
    '''
    Category Model Definition
    '''
    class Config:
        schema_extra = {
            'example': {
                'name': 'Apple',
                'color': '#55F47B',
            }
        }


class Filters(BaseModel):
    skip: int = 0
    limit: int = None
    search: str = None
    order_by: models.SortEnum = models.SortEnum.name
    order_by_type: models.SortTypeEnum = models.SortTypeEnum.ASC
