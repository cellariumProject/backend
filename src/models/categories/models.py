# Import library
from tortoise import fields, models
from enum import Enum


class SortTypeEnum(str, Enum):
    ASC = "ASC"
    DESC = "DESC"


class SortEnum(str, Enum):
    id = "id"
    name = "name"
    color = "color"


class Category(models.Model):
    """
    Category Database Definition
    """

    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=250, null=False, unique=True)
    color = fields.CharField(max_length=7, null=True)

    class PydanticMeta:
        exclude = ["items"]
