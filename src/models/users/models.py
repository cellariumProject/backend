# Import library
from tortoise import fields, models
from enum import Enum


class SortTypeEnum(str, Enum):
    ASC = "ASC"
    DESC = "DESC"


class SortEnum(str, Enum):
    id = "id"
    username = "username"
    email = "email"
    is_active = "is_active"
    is_admin = "is_admin"
    updated_at = "updated_at"
    created_at = "created_at"


class User(models.Model):
    """
    User Database Definition
    """

    id = fields.IntField(pk=True)
    username = fields.CharField(max_length=250, null=False, unique=True)
    email = fields.CharField(max_length=250, null=False, unique=True)
    password = fields.CharField(max_length=250, null=False)
    is_active = fields.BooleanField(default=True, null=False)
    is_admin = fields.BooleanField(default=False, null=False)
    updated_at = fields.DatetimeField(auto_now=True)
    created_at = fields.DatetimeField(auto_now_add=True)

    class PydanticMeta:
        exclude = ["password", "is_active", "is_admin", "user_items"]
