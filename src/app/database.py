# Import Library
from fastapi import FastAPI
from tortoise.contrib.fastapi import register_tortoise
from tortoise import Tortoise

from app.config import get_settings


# Get Settings
settings = get_settings()


# Database Connection Setup
if settings.database_host is None:
    TORTOISE_ORM = {
        "connections": {
            "default": settings.database_sqlite
        }
    }
else:
    TORTOISE_ORM = {
        "connections": {
            "default": {
                'engine': 'tortoise.backends.asyncpg',
                'credentials': {
                    'host': settings.database_host,
                    'port': settings.database_port,
                    'user': settings.database_user,
                    'password': settings.database_password,
                    'database': settings.database_db
                }
            }
        }
    }

# Database Models Setup
TORTOISE_ORM['apps'] = {
    "models": {
        "models": settings.database_models,
        "default_connection": "default",
    }
}


# Database Init
def init_db(app: FastAPI) -> None:
    """
    Init Database
    """
    register_tortoise(
        app,
        config=TORTOISE_ORM,
        generate_schemas=True,
        add_exception_handlers=True,
    )


def init_models():
    Tortoise.init_models(settings.database_models, "models")
