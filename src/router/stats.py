# Import library
from fastapi import APIRouter, Security, Response
from typing import List
from datetime import date

from models.stats import schemas
from models.stats import crud

from models.users.models import User
from models.security.crud import get_current_active_user


# Create router
router = APIRouter()


# Route definition
@router.get("/in_out_stock_count")
async def get_in_out_stock_count(
        response: Response,
        date_min: date,
        date_max: date,
        current_user: User = Security(get_current_active_user, scopes=["default"])):
    """
    Get Input Output Stock count between date.
    """
    db_data = await crud.get_in_out_count(
        user=current_user,
        date_min=date_min,
        date_max=date_max
    )
    response.headers["X-Total-Count"] = str(len(db_data))
    return db_data


@router.get("/waste_count", response_model=List[schemas.UserItemWasteCount])
async def get_waste_count_between_date(
        response: Response,
        date_min: date,
        date_max: date,
        current_user: User = Security(get_current_active_user, scopes=["default"])):
    """
    Get Waste count between date.
    """
    db_data = await crud.get_waste_count_between_date(
        user=current_user,
        date_min=date_min,
        date_max=date_max
    )
    response.headers["X-Total-Count"] = str(len(db_data))
    return db_data
