# Import library
from fastapi import APIRouter, Security, HTTPException, Response
from typing import List

from models.shopping_items import schemas
from models.shopping_items import crud
from models.shopping_items import models

from models.shopping_lists import crud as shoppingListsCrud

from models.users.models import User
from models.security.crud import get_current_active_user


# Create router
router = APIRouter()


# Route definition
"""
Shopping Items route definition
"""


@router.post("/byShoppingList/{shopping_list_id}/filters", response_model=schemas.ShoppingItemOutFilter)
async def get_all_with_advanced_filters(
        response: Response,
        shopping_list_id: int,
        filters: schemas.ShoppingItemFilter = schemas.ShoppingItemFilter(),
        current_user: User = Security(get_current_active_user, scopes=["default"])):
    """
    Get All Shopping Items with advanced filters.
    """
    db_data = await crud.get_all_by_user_by_shopping_list(
        user=current_user,
        shopping_list=await shoppingListsCrud.get(id=shopping_list_id),
        filters=filters)
    response.headers["X-Total-Count"] = str(db_data.nb_total_shopping_item)
    return db_data


@router.get("/byShoppingList/{shopping_list_id}", response_model=schemas.ShoppingItemOutFilter)
async def get_all_with_filters(
        response: Response,
        shopping_list_id: int,
        skip: int = None,
        limit: int = None,
        search: str = None,
        brand_id: int = None,
        unit_id: int = None,
        section_id: int = None,
        category_id: int = None,
        check: bool = None,
        order_by: models.SortEnum = None,
        order_by_type: models.SortTypeEnum = None,
        current_user: User = Security(get_current_active_user, scopes=["default"])):
    """
    Get All Shopping Items with filters.
    """
    filters = schemas.ShoppingItemFilter(
        skip=skip,
        limit=limit,
        search=search,
        check=check
    )

    if brand_id is not None:
        filters.brand_id_list = [brand_id]

    if unit_id is not None:
        filters.unit_id_list = [unit_id]

    if section_id is not None:
        filters.section_id_list = [section_id]

    if category_id is not None:
        filters.category_id_list = [category_id]

    if order_by is not None and order_by_type is not None:
        filters.order_by = [schemas.SortFilter(name=order_by, order=order_by_type)]

    db_data = await crud.get_all_by_user_by_shopping_list(
        user=current_user,
        shopping_list=await shoppingListsCrud.get(id=shopping_list_id),
        filters=filters)
    response.headers["X-Total-Count"] = str(db_data.nb_total_shopping_item)
    return db_data


@router.get("/byShoppingList/{shopping_list_id}/names", response_model=List[str])
async def get_all_names(
        response: Response,
        shopping_list_id: int,
        current_user: User = Security(get_current_active_user, scopes=["default"])):
    """
    Get All Shopping Items Names.
    """
    db_data = await crud.get_all_names_by_user_by_shopping_list(
        user=current_user,
        shopping_list=await shoppingListsCrud.get(id=shopping_list_id))
    response.headers["X-Total-Count"] = str(len(db_data))
    return db_data


@router.get("/{id}", response_model=schemas.ShoppingItemOut)
async def get(
        id: int,
        current_user: User = Security(get_current_active_user, scopes=["default"])):
    """
    Get Shopping Item by ID.
    """
    db_data = await crud.get_by_user(id=id, user=current_user)
    if db_data is None:
        raise HTTPException(status_code=404, detail="Shopping Item not found")
    return db_data


@router.post("/", response_model=schemas.ShoppingItemOut)
async def create(
        shopping_item: schemas.ShoppingItemIn,
        current_user: User = Security(get_current_active_user, scopes=["default"])):
    """
    Create Shopping Item.
    """
    db_data_list = await shoppingListsCrud.get(id=shopping_item.shopping_list_id)
    if db_data_list is None:
        raise HTTPException(status_code=404, detail="Shopping List not found")
    if db_data_list.user.id != current_user.id:
        raise HTTPException(status_code=401, detail="Not your shopping list !")
    db_data = await crud.create(shopping_item=shopping_item)
    if db_data is None:
        raise HTTPException(status_code=404, detail="Shopping Item not found")
    return db_data


@router.put("/{id}", response_model=schemas.ShoppingItemOut)
async def update(
        id: int,
        shopping_item: schemas.ShoppingItemIn,
        current_user: User = Security(get_current_active_user, scopes=["default"])):
    '''
    Update Shopping Item.
    '''
    db_data = await crud.get(id=id)
    if db_data is None:
        raise HTTPException(status_code=404, detail="Shopping Item not found")
    db_data_list = await shoppingListsCrud.get(id=db_data.shopping_list.id)
    if db_data_list is None:
        raise HTTPException(status_code=404, detail="Shopping List not found")
    if shopping_item.shopping_list_id != db_data.shopping_list.id or db_data_list.user.id != current_user.id:
        raise HTTPException(status_code=401, detail="Not your shopping list !")
    db_data = await crud.update(id=id, shopping_item=shopping_item)
    return db_data


@router.delete("/{id}", response_model=schemas.ShoppingItemOut)
async def delete(
        id: int,
        current_user: User = Security(get_current_active_user, scopes=["default"])):
    """
    Delete Shopping Item.
    """
    db_data = await crud.get(id=id)
    if db_data is None:
        raise HTTPException(status_code=404, detail="Shopping Item not found")
    db_data_list = await shoppingListsCrud.get(id=db_data.shopping_list.id)
    if db_data_list is None:
        raise HTTPException(status_code=404, detail="Shopping List not found")
    if db_data_list.user.id != current_user.id:
        raise HTTPException(status_code=401, detail="Not your shopping list !")
    await crud.delete(id=id)
    return db_data


@router.post("/faker", response_model=List[schemas.ShoppingItemOut])
async def faker(
        nb: int,
        shopping_list_id: int,
        current_user: User = Security(get_current_active_user, scopes=["default", "admin"])):
    """
    Generater fake Shopping Items.
    """
    db_data = await crud.faker(nb=nb, shopping_list_id=shopping_list_id)
    if db_data is None:
        raise HTTPException(status_code=404, detail="Shopping Items not found")
    return db_data
