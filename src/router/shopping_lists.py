# Import library
from fastapi import APIRouter, Security, HTTPException, Response
from typing import List

from models.shopping_lists import schemas
from models.shopping_lists import crud

from models.users.models import User
from models.security.crud import get_current_active_user


# Create router
router = APIRouter()


# Route definition
"""
Shopping Lists route definition
"""


@router.get("/", response_model=List[schemas.ShoppingListOut])
async def get_all(
        response: Response,
        skip: int = 0,
        limit: int = None,
        current_user: User = Security(get_current_active_user, scopes=["default"])):
    """
    Get All Shopping Lists with 3 Shopping Items.
    """
    db_data = await crud.get_all_by_user(user=current_user, skip=skip, limit=limit)
    response.headers["X-Total-Count"] = str(await crud.get_nb(user=current_user))
    return db_data


@router.get("/{id}", response_model=schemas.ShoppingListOut)
async def get(
        id: int,
        current_user: User = Security(get_current_active_user, scopes=["default"])):
    """
    Get Shopping List by ID.
    """
    db_data = await crud.get(id=id)
    if db_data is None:
        raise HTTPException(status_code=404, detail="Shopping List not found")
    if db_data.user.id != current_user.id:
        raise HTTPException(status_code=401, detail="Not your shopping list !")
    return db_data


@router.post("/", response_model=schemas.ShoppingListOut)
async def create(
        shopping_list: schemas.ShoppingListIn,
        current_user: User = Security(get_current_active_user, scopes=["default"])):
    """
    Create Shopping List.
    """
    if shopping_list.user_id != current_user.id:
        raise HTTPException(status_code=401, detail="Not your shopping list !")
    db_data = await crud.create(shopping_list=shopping_list)
    if db_data is None:
        raise HTTPException(status_code=404, detail="Shopping List not found")
    return db_data


@router.put("/{id}", response_model=schemas.ShoppingListOut)
async def update(
        id: int,
        shopping_list: schemas.ShoppingListIn,
        current_user: User = Security(get_current_active_user, scopes=["default"])):
    '''
    Update Shopping List.
    '''
    db_data = await crud.get(id=id)
    if db_data is None:
        raise HTTPException(status_code=404, detail="Shopping List not found")
    if shopping_list.user_id != current_user.id or db_data.user.id != current_user.id:
        raise HTTPException(status_code=401, detail="Not your shopping list !")
    db_data = await crud.update(id=id, shopping_list=shopping_list)
    return db_data


@router.delete("/{id}", response_model=schemas.ShoppingListOut)
async def delete(
        id: int,
        current_user: User = Security(get_current_active_user, scopes=["default"])):
    """
    Delete Shopping List.
    """
    db_data = await crud.get(id=id)
    if db_data is None:
        raise HTTPException(status_code=404, detail="Shopping List not found")
    if db_data.user.id != current_user.id:
        raise HTTPException(status_code=401, detail="Not your shopping list !")
    await crud.delete(id=id)
    return db_data


@router.post("/faker", response_model=List[schemas.ShoppingListOut])
async def faker(
        nb: int,
        current_user: User = Security(get_current_active_user, scopes=["default", "admin"])):
    """
    Generater fake Shopping List.
    """
    db_data = await crud.faker(nb=nb, user_id=current_user.id)
    if db_data is None:
        raise HTTPException(status_code=404, detail="Shopping List not found")
    return db_data
