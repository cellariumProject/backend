# Import library
from fastapi import APIRouter, Security, HTTPException, Response
from typing import Dict, List

from models.items import models
from models.items import schemas
from models.items import crud

from models.users.models import User
from models.security.crud import get_current_active_user


# Create router
router = APIRouter()


# Route definition
"""
Items route definition
"""


@router.post("/filters", response_model=schemas.ItemOutFilter)
async def get_all_with_advanced_filters(
        response: Response,
        filters: schemas.ItemFilter = schemas.ItemFilter(),
        current_user: User = Security(get_current_active_user, scopes=["default"])):
    """
    Get All Items with advanced filters.
    """
    db_data = await crud.get_all(filters=filters)
    response.headers["X-Total-Count"] = str(db_data.nb_total_item)
    return db_data


@router.get("/", response_model=List[schemas.ItemOut])
async def get_all_with_filters(
        response: Response,
        skip: int = None,
        limit: int = None,
        search: str = None,
        brand_id: int = None,
        unit_id: int = None,
        section_id: int = None,
        category_id: int = None,
        order_by: models.SortEnum = models.SortEnum.name,
        order_by_type: models.SortTypeEnum = models.SortTypeEnum.ASC,
        current_user: User = Security(get_current_active_user, scopes=["default"])):
    """
    Get All Items with filters.
    """

    filters = schemas.ItemFilter(
        skip=skip,
        limit=limit,
        search=search,
    )

    if brand_id is not None:
        filters.brand_id_list = [brand_id]

    if unit_id is not None:
        filters.unit_id_list = [unit_id]

    if section_id is not None:
        filters.section_id_list = [section_id]

    if category_id is not None:
        filters.category_id_list = [category_id]

    if order_by is not None and order_by_type is not None:
        filters.order_by = [schemas.SortFilter(name=order_by, order=order_by_type)]

    db_data = await crud.get_all(filters=filters)
    response.headers["X-Total-Count"] = str(db_data.nb_total_item)
    return db_data.items


@router.get("/names", response_model=List[str])
async def get_all_names(current_user: User = Security(get_current_active_user, scopes=["default"])):
    """
    Get All Items Names.
    """
    return await crud.get_all_names()


@router.get("/{id}", response_model=schemas.ItemOut)
async def get(
        id: int,
        current_user: User = Security(get_current_active_user, scopes=["default"])):
    """
    Get Item by ID.
    """
    db_data = await crud.get(id=id)
    if db_data is None:
        raise HTTPException(status_code=404, detail="Item not found")
    return db_data


@router.post("/", response_model=schemas.ItemOut)
async def create(
        item: schemas.ItemIn,
        current_user: User = Security(get_current_active_user, scopes=["default", "admin"])):
    """
    Create Item.
    """
    db_data = await crud.get_if_exist(item=item)
    if db_data is not None:
        raise HTTPException(status_code=404, detail="Item already exist")
    db_data = await crud.create(item=item)
    if db_data is None:
        raise HTTPException(status_code=404, detail="Item not found")
    return db_data


@router.put("/{id}", response_model=schemas.ItemOut)
async def update(
        id: int,
        item: schemas.ItemIn,
        current_user: User = Security(get_current_active_user, scopes=["default", "admin"])):
    '''
    Update User.
    '''
    db_data = await crud.get(id=id)
    if db_data is None:
        raise HTTPException(status_code=404, detail="Item not found")
    db_data = await crud.update(id=id, item=item)
    return db_data


@router.delete("/{id}", response_model=schemas.ItemOut)
async def delete(
        id: int,
        current_user: User = Security(get_current_active_user, scopes=["default", "admin"])):
    """
    Delete Item.
    """
    db_data = await crud.get(id=id)
    if db_data is None:
        raise HTTPException(status_code=404, detail="Item not found")
    await crud.delete(id=id)
    return db_data


@router.post("/faker", response_model=List[schemas.ItemOut])
async def faker(
        nb: int,
        current_user: User = Security(get_current_active_user, scopes=["default", "admin"])):
    """
    Generater fake Items.
    """
    db_data = await crud.faker(nb=nb)
    if db_data is None:
        raise HTTPException(status_code=404, detail="Items not found")
    return db_data


"""
Enum route definition
"""


@router.get("/enum/sort", response_model=Dict[str, str])
async def get_enum_sort():
    """
    Get Enum Sort.
    """
    return {i.name: i.value for i in models.SortEnum}
