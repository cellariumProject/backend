# Import library
from fastapi import APIRouter, Security, HTTPException, Response
from typing import List
from datetime import date

from models.user_items import schemas
from models.user_items import crud
from models.user_items import models

from models.users.models import User
from models.security.crud import get_current_active_user

# Create router
router = APIRouter()


# Route definition
"""
User items route definition
"""


@router.post("/filters", response_model=schemas.UserItemOutFilter)
async def get_all_by_user_with_advanced_filters(
        response: Response,
        filters: schemas.UserItemFilter = schemas.UserItemFilter(),
        current_user: User = Security(get_current_active_user, scopes=["default"])):
    """
    Get All User Items with advanced filters.
    """
    db_data = await crud.get_all_by_user(user=current_user, filters=filters)
    response.headers["X-Total-Count"] = str(db_data.nb_total_user_item)
    return db_data


@router.get("/", response_model=schemas.UserItemOutFilter)
async def get_all_by_user_with_filters(
        response: Response,
        skip: int = None,
        limit: int = None,
        search: str = None,
        brand_id: int = None,
        unit_id: int = None,
        section_id: int = None,
        category_id: int = None,
        wasted: bool = None,
        deleted: bool = None,
        expire_at: date = None,
        order_by: models.SortEnum = None,
        order_by_type: models.SortTypeEnum = None,
        current_user: User = Security(get_current_active_user, scopes=["default"])):
    """
    Get All User Items.
    """
    filters = schemas.UserItemFilter(
        skip=skip,
        limit=limit,
        search=search,
        wasted=wasted,
        deleted=deleted
    )

    if expire_at is not None:
        filters.expire_at = expire_at

    if brand_id is not None:
        filters.brand_id_list = [brand_id]

    if unit_id is not None:
        filters.unit_id_list = [unit_id]

    if section_id is not None:
        filters.section_id_list = [section_id]

    if category_id is not None:
        filters.category_id_list = [category_id]

    if order_by is not None and order_by_type is not None:
        filters.order_by = [schemas.SortFilter(name=order_by, order=order_by_type)]

    db_data = await crud.get_all_by_user(user=current_user, filters=filters)
    response.headers["X-Total-Count"] = str(db_data.nb_total_user_item)
    return db_data


@router.get("/names", response_model=List[str])
async def get_all_names_by_user(current_user: User = Security(get_current_active_user, scopes=["default"])):
    """
    Get All User Items Names.
    """
    return await crud.get_all_names_by_user(user=current_user)


@router.get("/{id}", response_model=schemas.UserItemOut)
async def get(
        id: int,
        current_user: User = Security(get_current_active_user, scopes=["default"])):
    """
    Get User Item by ID.
    """
    db_data = await crud.get(id=id)
    if db_data is None:
        raise HTTPException(status_code=404, detail="User Item not found")
    if db_data.user.id != current_user.id:
        raise HTTPException(status_code=401, detail="User is not owner of user item")
    return db_data


@router.post("/", response_model=schemas.UserItemOut)
async def create(
        user_item: schemas.UserItemCreate,
        current_user: User = Security(get_current_active_user, scopes=["default"])):
    """
    Create User Item.
    """
    if user_item.user_id != current_user.id:
        raise HTTPException(status_code=401, detail="User is not owner of user item")
    db_data = await crud.create(user_item=user_item)
    if db_data is None:
        raise HTTPException(status_code=404, detail="User item not found")
    return db_data


@router.put("/{id}", response_model=schemas.UserItemOut)
async def update(
        id: int,
        user_item: schemas.UserItemBase,
        current_user: User = Security(get_current_active_user, scopes=["default"])):
    """
    Update User Item.
    """
    db_data = await crud.get(id=id)
    if db_data is None:
        raise HTTPException(status_code=404, detail="User item not found")
    if db_data.user.id != current_user.id:
        raise HTTPException(status_code=401, detail="User is not owner of user item")
    db_data = await crud.update(id=id, user_item=user_item)
    return db_data


@router.delete("/{id}", response_model=schemas.UserItemOut)
async def delete(
        id: int,
        current_user: User = Security(get_current_active_user, scopes=["default"])):
    """
    Delete Item.
    """
    db_data = await crud.get(id=id)
    if db_data is None:
        raise HTTPException(status_code=404, detail="User item not found")
    if db_data.user.id != current_user.id:
        raise HTTPException(status_code=401, detail="User is not owner of user item")
    await crud.delete(id=id)
    return db_data


@router.post("/faker", response_model=List[schemas.UserItemOut])
async def faker(
        nb: int,
        current_user: User = Security(get_current_active_user, scopes=["default", "admin"])):
    """
    Generater fake User Items.
    """
    db_data = await crud.faker(nb=nb, user_id=current_user.id)
    if db_data is None:
        raise HTTPException(status_code=404, detail="User Items not found")
    return db_data
