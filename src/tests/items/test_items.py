# Import library
import pytest
from httpx import AsyncClient
from faker import Faker
from random import randint

from models.items import models


"""
This file contain the tests related to the items routes.
"""


# False Data Setup
faker = Faker()


# Test Variable of item before the update
pytest.item_before = {
    'id': None,
    'name': faker.word(),
    'img_path': faker.image_url(),
    'brand': {
        'id': None,
        'name': faker.word(),
    },
    'default_quantity': randint(1, 10),
    'unit': {
        'id': None,
        'name': faker.word(),
    },
    'section': {
        'id': None,
        'name': faker.word(),
    },
    'category': {
        'id': None,
        'name': faker.word(),
    },
    'brand_id': None,
    'unit_id': None,
    'section_id': None,
    'category_id': None,
}

# Test Variable of item after the update
pytest.item_after = {
    'id': None,
    'name': faker.word(),
    'img_path': faker.image_url(),
    'brand': {
        'id': None,
        'name': faker.word(),
    },
    'default_quantity': randint(1, 10),
    'unit': {
        'id': None,
        'name': faker.word(),
    },
    'section': {
        'id': None,
        'name': faker.word(),
    },
    'category': {
        'id': None,
        'name': faker.word(),
    },
    'brand_id': None,
    'unit_id': None,
    'section_id': None,
    'category_id': None,
}

pytest.item_sort_enum = {i.name: i.value for i in models.SortEnum}


# Test Definition
@pytest.mark.asyncio
async def test_create_brand(client: AsyncClient):
    """
    Given a name,
    When we execute POST on /brands/,
    Then app shall return status 200 with the data of the new brand.
    """
    response = await client.post(
        "/brands/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "name": pytest.item_before.get('brand').get('name'),
        })
    assert response.status_code == 200

    pytest.item_before['brand'] = response.json()
    pytest.item_before['brand_id'] = response.json()["id"]
    pytest.item_after['brand'] = response.json()
    pytest.item_after['brand_id'] = response.json()["id"]

    assert response.json() == pytest.item_before.get('brand')


@pytest.mark.asyncio
async def test_create_unit(client: AsyncClient):
    """
    Given a name,
    When we execute POST on /units/,
    Then app shall return status 200 with the data of the new unit.
    """
    response = await client.post(
        "/units/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "name": pytest.item_before.get('unit').get('name'),
        })
    assert response.status_code == 200

    pytest.item_before['unit'] = response.json()
    pytest.item_before['unit_id'] = response.json()["id"]
    pytest.item_after['unit'] = response.json()
    pytest.item_after['unit_id'] = response.json()["id"]

    assert response.json() == pytest.item_before.get('unit')


@pytest.mark.asyncio
async def test_create_section(client: AsyncClient):
    """
    Given a name,
    When we execute POST on /sections/,
    Then app shall return status 200 with the data of the new section.
    """
    response = await client.post(
        "/sections/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "name": pytest.item_before.get('section').get('name'),
        })
    assert response.status_code == 200

    pytest.item_before['section'] = response.json()
    pytest.item_before['section_id'] = response.json()["id"]
    pytest.item_after['section'] = response.json()
    pytest.item_after['section_id'] = response.json()["id"]

    assert response.json() == pytest.item_before.get('section')


@pytest.mark.asyncio
async def test_create_category(client: AsyncClient):
    """
    Given a name,
    When we execute POST on /categories/,
    Then app shall return status 200 with the data of the new category.
    """
    response = await client.post(
        "/categories/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "name": pytest.item_before.get('category').get('name'),
        })
    assert response.status_code == 200

    pytest.item_before['category'] = response.json()
    pytest.item_before['category_id'] = response.json()["id"]
    pytest.item_after['category'] = response.json()
    pytest.item_after['category_id'] = response.json()["id"]

    assert response.json() == pytest.item_before.get('category')


@pytest.mark.asyncio
async def test_create(client: AsyncClient):
    """
    Given a name,
    When we execute POST on /items/,
    Then app shall return status 200 with the data of the new item.
    """
    response = await client.post(
        "/items/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "name": pytest.item_before.get('name'),
            "img_path": pytest.item_before.get('img_path'),
            "brand_id": pytest.item_before.get('brand').get('id'),
            "default_quantity": pytest.item_before.get('default_quantity'),
            "unit_id": pytest.item_before.get('unit').get('id'),
            "section_id": pytest.item_before.get('section').get('id'),
            "category_id": pytest.item_before.get('category').get('id'),
        })
    assert response.status_code == 200

    pytest.item_before['id'] = response.json().get('id')

    pytest.item_after['id'] = response.json().get('id')

    assert response.json() == pytest.item_before


@pytest.mark.asyncio
async def test_get_all_01(client: AsyncClient):
    """
    Given nothing,
    When we execute GET on /items/ and at least one item exist,
    Then app shall return status 200 with a list of all the items.
    """
    response = await client.get(
        "/items/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_default)})
    assert response.status_code == 200
    assert pytest.item_before in response.json()


@pytest.mark.asyncio
async def test_get_one_by_id_01(client: AsyncClient):
    """
    Given an ID,
    When we execute GET on /items/{ID} and the item exist,
    Then app shall return status 200 with the data of the item found .
    """
    response = await client.get(
        "/items/{}".format(pytest.item_before.get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_default)})
    assert response.status_code == 200
    assert response.json() == pytest.item_before


@pytest.mark.asyncio
async def test_update_01(client: AsyncClient):
    """
    Given an id with name,
    When we execute PUT on /items/{ID} and the item exist,
    Then app shall return status 200 with the data of the item and update its data in the DB.
    """
    response = await client.put(
        "/items/{}".format(pytest.item_after.get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "name": pytest.item_after.get('name'),
            "img_path": pytest.item_after.get('img_path'),
            "brand_id": pytest.item_after.get('brand').get('id'),
            "default_quantity": pytest.item_after.get('default_quantity'),
            "unit_id": pytest.item_after.get('unit').get('id'),
            "section_id": pytest.item_after.get('section').get('id'),
            "category_id": pytest.item_after.get('category').get('id'),
        })
    assert response.status_code == 200
    assert response.json() == pytest.item_after


@pytest.mark.asyncio
async def test_get_all_02(client: AsyncClient):
    """
    Given nothing,
    When we execute GET on /items/,
    Then app shall return status 200 with a list of all the items.
    """
    response = await client.get(
        "/items/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_default)})
    assert response.status_code == 200
    assert pytest.item_after in response.json()


@pytest.mark.asyncio
async def test_get_one_by_id_02(client: AsyncClient):
    """
    Given an ID,
    When we execute GET on /items/{ID},
    Then app shall return status 200 with the data of the item found.
    """
    response = await client.get(
        "/items/{}".format(pytest.item_after.get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_default)})
    assert response.status_code == 200
    assert response.json() == pytest.item_after


@pytest.mark.asyncio
async def test_delete(client: AsyncClient):
    """
    Given an ID,
    When we execute DELETE on /items/{ID} and the item exist,
    Then app shall return status 200 with the data of the item and delete the item in the DB.
    """
    response = await client.delete(
        "/items/{}".format(pytest.item_after.get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)})
    assert response.status_code == 200
    assert response.json() == pytest.item_after


@pytest.mark.asyncio
async def test_get_nothing_by_id_01(client: AsyncClient):
    """
    Given an ID,
    When we execute GET on /items/{ID} and the item doesn't exist,
    Then app shall return status 404.
    """
    response = await client.get(
        "/items/{}".format(pytest.item_after.get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_default)})
    assert response.status_code == 404


@pytest.mark.asyncio
async def test_delete_brand(client: AsyncClient):
    """
    Given an ID,
    When we execute DELETE on /brands/{ID} and the brand exist,
    Then app shall return status 200 with the data of the brand and delete the brand in the DB.
    """
    response = await client.delete(
        "/brands/{}".format(pytest.item_after.get('brand').get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)})
    assert response.status_code == 200
    assert response.json() == pytest.item_after.get('brand')


@pytest.mark.asyncio
async def test_delete_unit(client: AsyncClient):
    """
    Given an ID,
    When we execute DELETE on /units/{ID} and the unit exist,
    Then app shall return status 200 with the data of the unit and delete the unit in the DB.
    """
    response = await client.delete(
        "/units/{}".format(pytest.item_after.get('unit').get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)})
    assert response.status_code == 200
    assert response.json() == pytest.item_after.get('unit')


@pytest.mark.asyncio
async def test_delete_section(client: AsyncClient):
    """
    Given an ID,
    When we execute DELETE on /sections/{ID} and the section exist,
    Then app shall return status 200 with the data of the section and delete the section in the DB.
    """
    response = await client.delete(
        "/sections/{}".format(pytest.item_after.get('section').get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)})
    assert response.status_code == 200
    assert response.json() == pytest.item_after.get('section')


@pytest.mark.asyncio
async def test_delete_category(client: AsyncClient):
    """
    Given an ID,
    When we execute DELETE on /categories/{ID} and the category exist,
    Then app shall return status 200 with the data of the category and delete the category in the DB.
    """
    response = await client.delete(
        "/categories/{}".format(pytest.item_after.get('category').get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)})
    assert response.status_code == 200
    assert response.json() == pytest.item_after.get('category')


@pytest.mark.asyncio
async def test_get_enum_sort(client: AsyncClient):
    """
    Given nothing,
    When we execute GET on /items/enum/sort,
    Then app shall return status 200 with the data of the sort enum.
    """
    response = await client.get("/items/enum/sort")
    assert response.status_code == 200
    assert response.json() == pytest.item_sort_enum
