# Import library
import pytest
from httpx import AsyncClient
from faker import Faker
from random import randint, uniform

"""
This file contain the tests related to the shopping items routes.
"""

# False Data Setup
faker = Faker()

pytest.shopping_list_01 = {
    "id": None,
    "name": faker.word(),
    "user": {
        "id": 1,  # id du root
        "username": faker.word(),
        "email": faker.word(),
        "updated_at": None,
        "created_at": None,
    },
    "shopping_items": [],
}
pytest.shopping_list_02 = {
    "id": None,
    "name": faker.word(),
    "user": {
        "id": 1,  # id du root
        "username": faker.word(),
        "email": faker.word(),
        "updated_at": None,
        "created_at": None,
    },
    "shopping_items": [],
}

pytest.shopping_item_before = {
    "id": None,
    "quantity": round(uniform(1, 10)),
    "check": False,
    "created_at": faker.date(),
    "item": {
        "id": None,
        "name": faker.word(),
        "img_path": faker.image_url(),
        "brand": {
            "id": None,
            "name": faker.word(),
        },
        "default_quantity": randint(1, 10),
        "unit": {
            "id": None,
            "name": faker.word(),
        },
        "section": {
            "id": None,
            "name": faker.word(),
        },
        "category": {
            "id": None,
            "name": faker.word(),
        },
        "brand_id": None,
        "unit_id": None,
        "section_id": None,
        "category_id": None,
    },
    "shopping_list": {
        "id": None,
        "name": faker.word(),
        "user": {
            "id": 1,  # id du root
            "username": faker.word(),
            "email": faker.word(),
            "updated_at": None,
            "created_at": None,
        },
    },
}

pytest.shopping_item_after = {
    "id": None,
    "quantity": round(uniform(1, 10)),
    "check": True,
    "created_at": faker.date(),
    "item": {
        "id": None,
        "name": faker.word(),
        "img_path": faker.image_url(),
        "brand": {
            "id": None,
            "name": faker.word(),
        },
        "default_quantity": randint(1, 10),
        "unit": {
            "id": None,
            "name": faker.word(),
        },
        "section": {
            "id": None,
            "name": faker.word(),
        },
        "category": {
            "id": None,
            "name": faker.word(),
        },
        "brand_id": None,
        "unit_id": None,
        "section_id": None,
        "category_id": None,
    },
    "shopping_list": {
        "id": None,
        "name": faker.word(),
        "user": {
            "id": 1,  # id du root
            "username": faker.word(),
            "email": faker.word(),
            "updated_at": None,
            "created_at": None,
        },
    },
}


def without_keys(d, keys):
    return {x: d[x] for x in d if x not in keys}


# Assign user
@pytest.mark.asyncio
async def test_get_current_user(client: AsyncClient):
    """
    Given a user token,
    When we try to get our user informations,
    Then app shall return status 200 and our valid user,
    """
    response = await client.get(
        "/users/my",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
    )

    assert response.status_code == 200

    pytest.shopping_list_01["user"] = response.json()
    pytest.shopping_list_02["user"] = response.json()

    pytest.shopping_item_before["shopping_list"]["user"] = response.json()
    pytest.shopping_item_after["shopping_list"]["user"] = response.json()


# Create shopping_list
@pytest.mark.asyncio
async def test_create_shopping_list(client: AsyncClient):
    """
    Given a name and a user_id,
    When we execute POST on /shopping_lists/,
    Then app shall return status 200 with the data of the new shopping_list.
    """
    response = await client.post(
        "/shopping_lists/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "name": pytest.shopping_list_01.get("name"),
            "user_id": pytest.shopping_list_01.get("user").get("id"),
        },
    )
    assert response.status_code == 200

    pytest.shopping_list_01["id"] = response.json()["id"]
    pytest.shopping_list_02["id"] = response.json()["id"]
    pytest.shopping_item_before["shopping_list"]["id"] = response.json()["id"]
    pytest.shopping_item_before["shopping_list"]["name"] = response.json()["name"]
    pytest.shopping_item_after["shopping_list"]["id"] = response.json()["id"]
    pytest.shopping_item_after["shopping_list"]["name"] = response.json()["name"]

    assert response.status_code == 200
    assert response.json() == pytest.shopping_list_01


@pytest.mark.asyncio
async def test_get_shopping_lists_by_id_01(client: AsyncClient):
    """
    Given an ID,
    When we execute GET on /shopping_lists/{ID},
    Then app shall return status 200 with the data of the shopping_list found.
    """
    response = await client.get(
        "/shopping_lists/{}".format(pytest.shopping_list_01.get("id")),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
    )
    assert response.status_code == 200

    assert response.json() == pytest.shopping_list_01


# Test Definition
@pytest.mark.asyncio
async def test_create_brand_before(client: AsyncClient):
    """
    Given a name,
    When we execute POST on /brands/,
    Then app shall return status 200 with the data of the new brand.
    """
    response = await client.post(
        "/brands/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "name": pytest.shopping_item_before.get("item").get("brand").get("name"),
        },
    )
    assert response.status_code == 200

    pytest.shopping_item_before["item"]["brand"] = response.json()
    pytest.shopping_item_before["item"]["brand_id"] = response.json()["id"]

    assert response.json() == pytest.shopping_item_before.get("item").get("brand")


# Test Definition
@pytest.mark.asyncio
async def test_create_brand_after(client: AsyncClient):
    """
    Given a name,
    When we execute POST on /brands/,
    Then app shall return status 200 with the data of the new brand.
    """
    response = await client.post(
        "/brands/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "name": pytest.shopping_item_after.get("item").get("brand").get("name"),
        },
    )
    assert response.status_code == 200

    pytest.shopping_item_after["item"]["brand"] = response.json()
    pytest.shopping_item_after["item"]["brand_id"] = response.json()["id"]

    assert response.json() == pytest.shopping_item_after.get("item").get("brand")


@pytest.mark.asyncio
async def test_create_unit_before(client: AsyncClient):
    """
    Given a name,
    When we execute POST on /units/,
    Then app shall return status 200 with the data of the new unit.
    """
    response = await client.post(
        "/units/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "name": pytest.shopping_item_before.get("item").get("unit").get("name"),
        },
    )
    assert response.status_code == 200

    pytest.shopping_item_before["item"]["unit"] = response.json()
    pytest.shopping_item_before["item"]["unit_id"] = response.json()["id"]

    assert response.json() == pytest.shopping_item_before.get("item").get("unit")


@pytest.mark.asyncio
async def test_create_unit_after(client: AsyncClient):
    """
    Given a name,
    When we execute POST on /units/,
    Then app shall return status 200 with the data of the new unit.
    """
    response = await client.post(
        "/units/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "name": pytest.shopping_item_after.get("item").get("unit").get("name"),
        },
    )
    assert response.status_code == 200

    pytest.shopping_item_after["item"]["unit"] = response.json()
    pytest.shopping_item_after["item"]["unit_id"] = response.json()["id"]

    assert response.json() == pytest.shopping_item_after.get("item").get("unit")


@pytest.mark.asyncio
async def test_create_section_before(client: AsyncClient):
    """
    Given a name,
    When we execute POST on /sections/,
    Then app shall return status 200 with the data of the new section.
    """
    response = await client.post(
        "/sections/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "name": pytest.shopping_item_before.get("item").get("section").get("name"),
        },
    )
    assert response.status_code == 200

    pytest.shopping_item_before["item"]["section"] = response.json()
    pytest.shopping_item_before["item"]["section_id"] = response.json()["id"]

    assert response.json() == pytest.shopping_item_before.get("item").get("section")


@pytest.mark.asyncio
async def test_create_section_after(client: AsyncClient):
    """
    Given a name,
    When we execute POST on /sections/,
    Then app shall return status 200 with the data of the new section.
    """
    response = await client.post(
        "/sections/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "name": pytest.shopping_item_after.get("item").get("section").get("name"),
        },
    )
    assert response.status_code == 200

    pytest.shopping_item_after["item"]["section"] = response.json()
    pytest.shopping_item_after["item"]["section_id"] = response.json()["id"]

    assert response.json() == pytest.shopping_item_after.get("item").get("section")


@pytest.mark.asyncio
async def test_create_category_before(client: AsyncClient):
    """
    Given a name,
    When we execute POST on /categories/,
    Then app shall return status 200 with the data of the new category.
    """
    response = await client.post(
        "/categories/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "name": pytest.shopping_item_before.get("item").get("category").get("name"),
        },
    )
    assert response.status_code == 200

    pytest.shopping_item_before["item"]["category"] = response.json()
    pytest.shopping_item_before["item"]["category_id"] = response.json()["id"]

    assert response.json() == pytest.shopping_item_before.get("item").get("category")


@pytest.mark.asyncio
async def test_create_category_after(client: AsyncClient):
    """
    Given a name,
    When we execute POST on /categories/,
    Then app shall return status 200 with the data of the new category.
    """
    response = await client.post(
        "/categories/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "name": pytest.shopping_item_after.get("item").get("category").get("name"),
        },
    )
    assert response.status_code == 200

    pytest.shopping_item_after["item"]["category"] = response.json()
    pytest.shopping_item_after["item"]["category_id"] = response.json()["id"]

    assert response.json() == pytest.shopping_item_after.get("item").get("category")


@pytest.mark.asyncio
async def test_create_item_before(client: AsyncClient):
    """
    Given a name,
    When we execute POST on /items/,
    Then app shall return status 200 with the data of the new item.
    """
    response = await client.post(
        "/items/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "name": pytest.shopping_item_before.get("item").get("name"),
            "img_path": pytest.shopping_item_before.get("item").get("img_path"),
            "brand_id": pytest.shopping_item_before.get("item").get("brand").get("id"),
            "default_quantity": pytest.shopping_item_before.get("item").get(
                "default_quantity"
            ),
            "unit_id": pytest.shopping_item_before.get("item").get("unit").get("id"),
            "section_id": pytest.shopping_item_before.get("item")
            .get("section")
            .get("id"),
            "category_id": pytest.shopping_item_before.get("item")
            .get("category")
            .get("id"),
        },
    )
    assert response.status_code == 200

    pytest.shopping_item_before["item"]["id"] = response.json().get("id")
    pytest.shopping_item_before["item"]["name"] = response.json().get("name")
    pytest.shopping_item_before["item"]["img_path"] = response.json().get("img_path")
    pytest.shopping_item_before["item"]["default_quantity"] = response.json().get(
        "default_quantity"
    )

    assert response.json() == pytest.shopping_item_before.get("item")


@pytest.mark.asyncio
async def test_create_item_after(client: AsyncClient):
    """
    Given a name,
    When we execute POST on /items/,
    Then app shall return status 200 with the data of the new item.
    """
    response = await client.post(
        "/items/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "name": pytest.shopping_item_after.get("item").get("name"),
            "img_path": pytest.shopping_item_after.get("item").get("img_path"),
            "brand_id": pytest.shopping_item_after.get("item").get("brand").get("id"),
            "default_quantity": pytest.shopping_item_after.get("item").get(
                "default_quantity"
            ),
            "unit_id": pytest.shopping_item_after.get("item").get("unit").get("id"),
            "section_id": pytest.shopping_item_after.get("item")
            .get("section")
            .get("id"),
            "category_id": pytest.shopping_item_after.get("item")
            .get("category")
            .get("id"),
        },
    )
    assert response.status_code == 200

    pytest.shopping_item_after["item"]["id"] = response.json().get("id")
    pytest.shopping_item_after["item"]["name"] = response.json().get("name")
    pytest.shopping_item_after["item"]["img_path"] = response.json().get("img_path")
    pytest.shopping_item_after["item"]["default_quantity"] = response.json().get(
        "default_quantity"
    )

    assert response.json() == pytest.shopping_item_after.get("item")


@pytest.mark.asyncio
async def test_create_shopping_item(client: AsyncClient):
    """
    Given a name,
    When we execute POST on /shopping_items/,
    Then app shall return status 200 with the data of the new item.
    """
    response = await client.post(
        "/shopping_items/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "quantity": pytest.shopping_item_before.get("quantity"),
            "check": pytest.shopping_item_before.get("check"),
            "created_at": pytest.shopping_item_before.get("created_at"),
            "item_id": pytest.shopping_item_before.get("item").get("id"),
            "shopping_list_id": pytest.shopping_item_before.get("shopping_list").get(
                "id"
            ),
        },
    )

    assert response.status_code == 200

    pytest.shopping_item_before["id"] = response.json().get("id")
    pytest.shopping_item_after["id"] = response.json().get("id")

    assert response.json() == pytest.shopping_item_before


@pytest.mark.asyncio
async def test_get_shopping_item_by_id_01(client: AsyncClient):
    """
    Given an ID,
    When we execute GET on /shopping_items/{ID} and the shopping_item exist,
    Then app shall return status 200 with the data of the shopping_item found .
    """
    response = await client.get(
        "/shopping_items/{}".format(pytest.shopping_item_before.get("id")),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
    )
    assert response.status_code == 200
    assert response.json() == pytest.shopping_item_before


@pytest.mark.asyncio
async def test_update_shopping_item_01(client: AsyncClient):
    """
    Given an id with name,
    When we execute PUT on /shopping_items/{ID} and the shopping_item exist,
    Then app shall return status 200 with the data of the shopping_item and update its data in the DB.
    """
    response = await client.put(
        "/shopping_items/{}".format(pytest.shopping_item_after.get("id")),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "quantity": pytest.shopping_item_after.get("quantity"),
            "check": pytest.shopping_item_after.get("check"),
            "created_at": pytest.shopping_item_after.get("created_at"),
            "item_id": pytest.shopping_item_after.get("item").get("id"),
            "shopping_list_id": pytest.shopping_item_after.get("shopping_list").get(
                "id"
            ),
        },
    )

    assert response.status_code == 200
    invalid = {"shopping_list"}
    pytest.shopping_list_02["shopping_items"].append(
        without_keys(response.json(), invalid)
    )
    assert response.json() == pytest.shopping_item_after


@pytest.mark.asyncio
async def test_get_shopping_item_by_id_02(client: AsyncClient):
    """
    Given an ID,
    When we execute GET on /shopping_items/{ID},
    Then app shall return status 200 with the data of the unit found.
    """
    response = await client.get(
        "/shopping_items/{}".format(pytest.shopping_item_after.get("id")),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
    )
    assert response.status_code == 200
    assert response.json() == pytest.shopping_item_after


@pytest.mark.asyncio
async def test_update_shopping_lists(client: AsyncClient):
    """
    Given an id with name,
    When we execute PUT on /shopping_lists/{ID} and the shopping_item exist,
    Then app shall return status 200 with the data of the shopping_item and update its data in the DB.
    """
    response = await client.put(
        "/shopping_lists/{}".format(pytest.shopping_list_01.get("id")),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "name": pytest.shopping_list_02.get("name"),
            "user_id": pytest.shopping_list_02.get("user").get("id"),
        },
    )

    assert response.status_code == 200
    pytest.shopping_item_after["shopping_list"]["name"] = response.json()["name"]
    assert response.json() == pytest.shopping_list_02


@pytest.mark.asyncio
async def test_delete_shopping_item(client: AsyncClient):
    """
    Given an ID,
    When we execute DELETE on /shopping_items/{ID} and the unit exist,
    Then app shall return status 200 with the data of the unit and delete the unit in the DB.
    """
    response = await client.delete(
        "/shopping_items/{}".format(pytest.shopping_item_after.get("id")),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
    )
    assert response.status_code == 200
    assert response.json() == pytest.shopping_item_after
    pytest.shopping_list_02["shopping_items"].pop(0)


@pytest.mark.asyncio
async def test_get_nothing_by_id_01(client: AsyncClient):
    """
    Given an ID,
    When we execute GET on /shopping_items/{ID} and the unit doesn't exist,
    Then app shall return status 404.
    """
    response = await client.get(
        "/shopping_items/{}".format(pytest.shopping_item_after.get("id")),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
    )
    assert response.status_code == 404


@pytest.mark.asyncio
async def test_delete_shopping_list(client: AsyncClient):
    """
    Given an ID,
    When we execute DELETE on /shopping_lists/{ID} and the unit exist,
    Then app shall return status 200 with the data of the unit and delete the unit in the DB.
    """
    response = await client.delete(
        "/shopping_lists/{}".format(pytest.shopping_list_02.get("id")),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
    )
    assert response.status_code == 200
    assert response.json() == pytest.shopping_list_02


@pytest.mark.asyncio
async def test_get_nothing_by_id_02(client: AsyncClient):
    """
    Given an ID,
    When we execute GET on /shopping_lists/{ID} and the unit doesn't exist,
    Then app shall return status 404.
    """
    response = await client.get(
        "/shopping_lists/{}".format(pytest.shopping_list_02.get("id")),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
    )
    assert response.status_code == 404
