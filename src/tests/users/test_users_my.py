# Import library
import pytest
from httpx import AsyncClient
from faker import Faker


"""
This file contain the tests related to the users routes allowed to default user.
"""


# False Data Setup
faker = Faker()


# Test Variable
pytest.user_default_before = {
    'id': None,
    'username': faker.word(),
    'email': faker.email(),
    'updated_at': None,
    'created_at': None
}

pytest.user_default_before_password = faker.word()

pytest.user_default_after = {
    'id': None,
    'username': faker.word(),
    'email': faker.email(),
    'updated_at': None,
    'created_at': None
}

pytest.user_default_after_password = faker.word()

pytest.user_default_scope = "default"

pytest.user_default_token = None


# Test Definition
@pytest.mark.asyncio
async def test_username_is_available_01(client: AsyncClient):
    """
    Given a username,
    When we check if the username is available,
    Then app shall return status 200 and True,
    """
    response = await client.get("/users/isUsernameAvailable/?username={}".format(
        pytest.user_default_before.get('username')))
    assert response.status_code == 200
    assert response.json() is True


@pytest.mark.asyncio
async def test_email_is_available_01(client: AsyncClient):
    """
    Given a email,
    When we check if the email is available,
    Then app shall return status 200 and True,
    """
    response = await client.get("/users/isEmailAvailable/?email={}".format(
        pytest.user_default_before.get('email')))
    assert response.status_code == 200
    assert response.json() is True


@pytest.mark.asyncio
async def test_register(client: AsyncClient):
    """
    Given a username, an email and a password,
    When we try to register,
    Then app shall return status 200 and valid user,
    """
    response = await client.post(
        "/users/register",
        json={
            "username": pytest.user_default_before.get('username'),
            "email": pytest.user_default_before.get('email'),
            "password": pytest.user_default_before_password
        })
    assert response.status_code == 200

    pytest.user_default_before['id'] = response.json().get('id')
    pytest.user_default_before['updated_at'] = response.json().get('updated_at')
    pytest.user_default_before['created_at'] = response.json().get('created_at')

    pytest.user_default_after['id'] = pytest.user_default_before['id']
    pytest.user_default_after['created_at'] = pytest.user_default_before['created_at']

    assert response.json() == pytest.user_default_before


@pytest.mark.asyncio
async def test_username_is_available_02(client: AsyncClient):
    """
    Given a username,
    When we check if the username is available,
    Then app shall return status 200 and False,
    """
    response = await client.get("/users/isUsernameAvailable/?username={}".format(
        pytest.user_default_before.get('username')))
    assert response.status_code == 200
    assert response.json() is False


@pytest.mark.asyncio
async def test_email_is_available_02(client: AsyncClient):
    """
    Given a email,
    When we check if the email is available,
    Then app shall return status 200 and False,
    """
    response = await client.get("/users/isEmailAvailable/?email={}".format(
        pytest.user_default_before.get('email')))
    assert response.status_code == 200
    assert response.json() is False


@pytest.mark.asyncio
async def test_login_01(client: AsyncClient):
    """
    Given a username, a password and permissions,
    When we try to login,
    Then app shall return status 200 and a valid token.
    """
    response = await client.post(
        "/security/token/username",
        data={
            "username": pytest.user_default_before.get('username'),
            "password": pytest.user_default_before_password,
            "scope": pytest.user_default_scope
        })
    assert response.status_code == 200
    pytest.user_default_token = response.json().get('access_token')


@pytest.mark.asyncio
async def test_get_my_01(client: AsyncClient):
    """
    Given a user token,
    When we try to get our user informations,
    Then app shall return status 200 and our valid user,
    """
    response = await client.get(
        "/users/my",
        headers={"Authorization": "Bearer {}".format(pytest.user_default_token)})
    assert response.status_code == 200
    assert response.json() == pytest.user_default_before


@pytest.mark.asyncio
async def test_update(client: AsyncClient):
    """
    Given a user token with valid username and email,
    When we try to update our user,
    Then app shall return status 200 and a valid updated user,
    """
    response = await client.put(
        "/users/my",
        headers={"Authorization": "Bearer {}".format(pytest.user_default_token)},
        json={
            "username": pytest.user_default_after.get('username'),
            "email": pytest.user_default_after.get('email')
        })
    assert response.status_code == 200

    pytest.user_default_after['updated_at'] = response.json().get('updated_at')

    assert response.json() == pytest.user_default_after


@pytest.mark.asyncio
async def test_username_is_available_03(client: AsyncClient):
    """
    Given a username,
    When we check if the username is available,
    Then app shall return status 200 and False,
    """
    response = await client.get("/users/isUsernameAvailable/?username={}".format(
        pytest.user_default_after.get('username')))
    assert response.status_code == 200
    assert response.json() is False


@pytest.mark.asyncio
async def test_email_is_available_03(client: AsyncClient):
    """
    Given a email,
    When we check if the email is available,
    Then app shall return status 200 and False,
    """
    response = await client.get("/users/isEmailAvailable/?email={}".format(
        pytest.user_default_after.get('email')))
    assert response.status_code == 200
    assert response.json() is False


@pytest.mark.asyncio
async def test_update_password(client: AsyncClient):
    """
    Given a user token with our old password and our new password,
    When we try to update our password,
    Then app shall return status 200 and a our user,
    """
    response = await client.put(
        "/users/my/password",
        headers={"Authorization": "Bearer {}".format(pytest.user_default_token)},
        json={
            "old_password": pytest.user_default_before_password,
            "new_password": pytest.user_default_after_password
        })
    assert response.status_code == 200

    pytest.user_default_after['updated_at'] = response.json().get('updated_at')

    assert response.json() == pytest.user_default_after


@pytest.mark.asyncio
async def test_get_my_02(client: AsyncClient):
    """
    Given a user token,
    When we try to get our user informations,
    Then app shall return status 200 and our valid user,
    """
    response = await client.get(
        "/users/my",
        headers={"Authorization": "Bearer {}".format(pytest.user_default_token)})
    assert response.status_code == 200
    assert response.json() == pytest.user_default_after


@pytest.mark.asyncio
async def test_delete(client: AsyncClient):
    """
    Given a user token,
    When we try to delete our user,
    Then ap shall return status 200 and our user,
    """
    response = await client.delete(
        "/users/my",
        headers={"Authorization": "Bearer {}".format(pytest.user_default_token)})
    assert response.status_code == 200
    assert response.json() == pytest.user_default_after


@pytest.mark.asyncio
async def test_username_is_available_04(client: AsyncClient):
    """
    Given a username,
    When we check if the username is available,
    Then app shall return status 200 and True,
    """
    response = await client.get("/users/isUsernameAvailable/?username={}".format(
        pytest.user_default_after.get('username')))
    assert response.status_code == 200
    assert response.json() is True


@pytest.mark.asyncio
async def test_email_is_available_04(client: AsyncClient):
    """
    Given a email,
    When we check if the email is available,
    Then app shall return status 200 and True,
    """
    response = await client.get("/users/isEmailAvailable/?email={}".format(
        pytest.user_default_after.get('email')))
    assert response.status_code == 200
    assert response.json() is True


@pytest.mark.asyncio
async def test_get_my_03(client: AsyncClient):
    """
    Given a user token,
    When we try to get our user informations,
    Then app shall return status 401 because user isn't exist anymore,
    """
    response = await client.get(
        "/users/my",
        headers={"Authorization": "Bearer {}".format(pytest.user_default_token)})
    assert response.status_code == 401
