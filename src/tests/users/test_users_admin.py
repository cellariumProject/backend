# Import library
import pytest
from httpx import AsyncClient
from faker import Faker


"""
This file contain the tests related to the users routes allowed only to the administrator.
"""


# False Data Setup
faker = Faker()


# Test Variable of admin user before the update
pytest.admin_before = {
    'id': None,
    'username': faker.word(),
    'email': faker.email(),
    'is_active': True,
    'is_admin': True,
    'updated_at': None,
    'created_at': None
}

pytest.admin_before_password = faker.word()

# Test Variable of admin user after the update
pytest.admin_after = {
    'id': None,
    'username': faker.word(),
    'email': faker.email(),
    'is_active': False,
    'is_admin': False,
    'updated_at': None,
    'created_at': None
}

pytest.admin_after_password = faker.word()

pytest.admin_before_scope = "default admin"

pytest.admin_token = None


# Test Definition
@pytest.mark.asyncio
async def test_create_admin(client: AsyncClient):
    """
    Given a username,an email,a password,an active flag and an admin flag,
    When we execute POST on /users/,
    Then app shall return status 200 with the data of the new admin.
    """
    response = await client.post(
        "/users/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "username": pytest.admin_before.get('username'),
            "email": pytest.admin_before.get('email'),
            "password": pytest.admin_before_password,
            "is_active": pytest.admin_before.get('is_active'),
            "is_admin": pytest.admin_before.get('is_admin')
        })
    assert response.status_code == 200

    pytest.admin_before['id'] = response.json().get('id')
    pytest.admin_before['updated_at'] = response.json().get('updated_at')
    pytest.admin_before['created_at'] = response.json().get('created_at')

    pytest.admin_after['id'] = response.json().get('id')
    pytest.admin_after['created_at'] = response.json().get('created_at')

    assert response.json() == pytest.admin_before


@pytest.mark.asyncio
async def test_login_admin_01(client: AsyncClient):
    """
    Given a username, a password and permissions,
    When we try to login,
    Then app shall return status 200 and a valid token.
    """
    response = await client.post(
        "/security/token/username",
        data={
            "username": pytest.admin_before.get('username'),
            "password": pytest.admin_before_password,
            "scope": pytest.admin_before_scope
        })
    assert response.status_code == 200
    # We shall use the new access token of the newly created admin
    pytest.admin_token = response.json().get('access_token')


@pytest.mark.asyncio
async def test_get_all_admin_01(client: AsyncClient):  # Test if admin is in the response when we do get_all
    """
    Given nothing,
    When we execute GET on /users/ and at least one user exist,
    Then app shall return status 200 with a list of all the users .
    """
    response = await client.get(
        "/users/",
        # To allow permission as admin
        headers={"Authorization": "Bearer {}".format(pytest.admin_token)})
    assert response.status_code == 200
    # ** is for concatenation of dictionnary
    assert pytest.admin_before in response.json()


@pytest.mark.asyncio
async def test_get_one_by_id_admin_01(client: AsyncClient):
    """
    Given an ID,
    When we execute GET on /users/{ID} and the user exist,
    Then app shall return status 200 with the data of the user found .
    """
    response = await client.get(
        "/users/{}".format(pytest.admin_before.get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.admin_token)})
    assert response.status_code == 200
    assert response.json() == pytest.admin_before


@pytest.mark.asyncio
async def test_get_one_by_username_admin_01(client: AsyncClient):
    """
    Given a username,
    When we execute GET on /users/byUsername/?username={username} and the user exist,
    Then app shall return status 200 with the data of the user found .
    """
    response = await client.get(
        "/users/byUsername/?username={}".format(pytest.admin_before.get('username')),
        headers={"Authorization": "Bearer {}".format(pytest.admin_token)})
    assert response.status_code == 200
    assert response.json() == pytest.admin_before


@pytest.mark.asyncio
async def test_get_one_by_email_admin_01(client: AsyncClient):
    """
    Given an email,
    When we execute GET on /users/byEmail/?email={email} and the user exist,
    Then app shall return status 200 with the data of the user found .
    """
    response = await client.get(
        "/users/byEmail/?email={}".format(pytest.admin_before.get('email')),
        headers={"Authorization": "Bearer {}".format(pytest.admin_token)})
    assert response.status_code == 200
    assert response.json() == pytest.admin_before


@pytest.mark.asyncio
async def test_update_admin_01(client: AsyncClient):
    """
    Given an id with username, an email, an active flag and an admin flag,
    When we execute PUT on /users/{ID} and the user exist,
    Then app shall return status 200 with the data of the user and update its data in the DB.
    """
    response = await client.put(
        "/users/{}".format(pytest.admin_after.get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.admin_token)},
        json={
            "username": pytest.admin_after.get('username'),
            "email": pytest.admin_after.get('email'),
            "is_active": pytest.admin_after.get('is_active'),
            "is_admin": pytest.admin_after.get('is_admin')
        })
    assert response.status_code == 200

    pytest.admin_after['updated_at'] = response.json().get('updated_at')

    assert response.json() == pytest.admin_after


@pytest.mark.asyncio
async def test_update_admin_02(client: AsyncClient):
    """
    Given an id with username, an email, an active flag and an admin flag,
    When we execute PUT on /users/{ID} and the user exist,
    Then app shall return status 200 with the data of the user and update its data in the DB.
    """
    pytest.admin_after['is_active'] = True
    pytest.admin_after['is_admin'] = True

    response = await client.put(
        "/users/{}".format(pytest.admin_after.get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "username": pytest.admin_after.get('username'),
            "email": pytest.admin_after.get('email'),
            "is_active": pytest.admin_after.get('is_active'),
            "is_admin": pytest.admin_after.get('is_admin')
        })
    assert response.status_code == 200

    pytest.admin_after['updated_at'] = response.json().get('updated_at')

    assert response.json() == pytest.admin_after


@pytest.mark.asyncio  # Verify the new Data from test_update_admin_01
async def test_get_all_admin_02(client: AsyncClient):  # Test if admin is in the response when we do get_all
    """
    Given nothing,
    When we execute GET on /users/ ,
    Then app shall return status 200 with a list of all the users .
    """
    response = await client.get(
        "/users/",
        headers={"Authorization": "Bearer {}".format(pytest.admin_token)})
    assert response.status_code == 200
    assert pytest.admin_after in response.json()


@pytest.mark.asyncio
async def test_get_one_by_id_admin_02(client: AsyncClient):
    """
    Given an ID,
    When we execute GET on /users/{ID} ,
    Then app shall return status 200 with the data of the user found .
    """
    response = await client.get(
        "/users/{}".format(pytest.admin_after.get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.admin_token)})
    assert response.status_code == 200
    assert response.json() == pytest.admin_after


@pytest.mark.asyncio
async def test_get_one_by_username_admin_02(client: AsyncClient):
    """
    Given a username,
    When we execute GET on /users/byUsername/?username={username} and the user exist,
    Then app shall return status 200 with the data of the user found .
    """
    response = await client.get(
        "/users/byUsername/?username={}".format(pytest.admin_after.get('username')),
        headers={"Authorization": "Bearer {}".format(pytest.admin_token)})
    assert response.status_code == 200
    assert response.json() == pytest.admin_after


@pytest.mark.asyncio
async def test_get_one_by_email_admin_02(client: AsyncClient):
    """
    Given an email,
    When we execute GET on /users/byEmail/?email={email} and the user exist,
    Then app shall return status 200 with the data of the user found .
    """
    response = await client.get(
        "/users/byEmail/?email={}".format(pytest.admin_after.get('email')),
        headers={"Authorization": "Bearer {}".format(pytest.admin_token)})
    assert response.status_code == 200
    assert response.json() == pytest.admin_after


@pytest.mark.asyncio
async def test_update_password_admin_01(client: AsyncClient):
    """
    Given an id with a new password,
    When we execute PUT on /users/{password}/password and the user exist,
    Then app shall return status 200 with the data of the user and update its password in the DB.
    """
    response = await client.put(
        "/users/{}/password".format(pytest.admin_after.get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.admin_token)},
        json={
            "new_password": pytest.admin_after_password,
        })
    assert response.status_code == 200
    assert response.json() == pytest.admin_after


@pytest.mark.asyncio
async def test_delete_admin(client: AsyncClient):
    """
    Given an ID,
    When we execute DELETE on /users/{ID} and the user exist,
    Then app shall return status 200 with the data of the user and delete the user in the DB.
    """
    response = await client.delete(
        "/users/{}".format(pytest.admin_after.get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.admin_token)})
    assert response.status_code == 200
    assert response.json() == pytest.admin_after


@pytest.mark.asyncio
async def test_get_nothing_by_id_01(client: AsyncClient):
    """
    Given an ID,
    When we execute GET on /users/{ID} and the user doesn't exist,
    Then app shall return status 404.
    """
    response = await client.get(
        "/users/{}".format(pytest.admin_after.get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)})
    assert response.status_code == 404
