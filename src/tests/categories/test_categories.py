# Import library
import pytest
from httpx import AsyncClient
from faker import Faker


"""
This file contain the tests related to the categories routes.
"""


# False Data Setup
faker = Faker()


# Test Variable of category before the update
pytest.category_before = {
    'id': None,
    'name': faker.word(),
    'color': faker.color()
}

# Test Variable of category after the update
pytest.category_after = {
    'id': None,
    'name': faker.word(),
    'color': faker.color()
}


# Test Definition
@pytest.mark.asyncio
async def test_create(client: AsyncClient):
    """
    Given a name and a color,
    When we execute POST on /categories/,
    Then app shall return status 200 with the data of the new category.
    """
    response = await client.post(
        "/categories/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "name": pytest.category_before.get('name'),
            "color": pytest.category_before.get('color'),
        })
    assert response.status_code == 200

    pytest.category_before['id'] = response.json().get('id')

    pytest.category_after['id'] = response.json().get('id')

    assert response.json() == pytest.category_before


@pytest.mark.asyncio
async def test_get_all_01(client: AsyncClient):
    """
    Given nothing,
    When we execute GET on /categories/ and at least one category exist,
    Then app shall return status 200 with a list of all the categories.
    """
    response = await client.get(
        "/categories/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_default)})
    assert response.status_code == 200
    assert pytest.category_before in response.json()


@pytest.mark.asyncio
async def test_get_one_by_id_01(client: AsyncClient):
    """
    Given an ID,
    When we execute GET on /categories/{ID} and the category exist,
    Then app shall return status 200 with the data of the category found .
    """
    response = await client.get(
        "/categories/{}".format(pytest.category_before.get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_default)})
    assert response.status_code == 200
    assert response.json() == pytest.category_before


@pytest.mark.asyncio
async def test_update_01(client: AsyncClient):
    """
    Given an id with name and color,
    When we execute PUT on /categories/{ID} and the category exist,
    Then app shall return status 200 with the data of the category and update its data in the DB.
    """
    response = await client.put(
        "/categories/{}".format(pytest.category_after.get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "name": pytest.category_after.get('name'),
            "color": pytest.category_after.get('color'),
        })
    assert response.status_code == 200
    assert response.json() == pytest.category_after


@pytest.mark.asyncio
async def test_get_all_02(client: AsyncClient):
    """
    Given nothing,
    When we execute GET on /categories/,
    Then app shall return status 200 with a list of all the categories.
    """
    response = await client.get(
        "/categories/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_default)})
    assert response.status_code == 200
    assert pytest.category_after in response.json()


@pytest.mark.asyncio
async def test_get_one_by_id_02(client: AsyncClient):
    """
    Given an ID,
    When we execute GET on /categories/{ID},
    Then app shall return status 200 with the data of the category found.
    """
    response = await client.get(
        "/categories/{}".format(pytest.category_after.get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_default)})
    assert response.status_code == 200
    assert response.json() == pytest.category_after


@pytest.mark.asyncio
async def test_delete(client: AsyncClient):
    """
    Given an ID,
    When we execute DELETE on /categories/{ID} and the category exist,
    Then app shall return status 200 with the data of the category and delete the category in the DB.
    """
    response = await client.delete(
        "/categories/{}".format(pytest.category_after.get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)})
    assert response.status_code == 200
    assert response.json() == pytest.category_after


@pytest.mark.asyncio
async def test_get_nothing_by_id_01(client: AsyncClient):
    """
    Given an ID,
    When we execute GET on /categories/{ID} and the category doesn't exist,
    Then app shall return status 404.
    """
    response = await client.get(
        "/categories/{}".format(pytest.category_after.get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_default)})
    assert response.status_code == 404
