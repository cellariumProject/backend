# Import library
import pytest
from httpx import AsyncClient
from faker import Faker


"""
This file contain the tests related to the sections routes.
"""


# False Data Setup
faker = Faker()


# Test Variable of section before the update
pytest.section_before = {
    'id': None,
    'name': faker.word(),
    'color': faker.color()
}

# Test Variable of section after the update
pytest.section_after = {
    'id': None,
    'name': faker.word(),
    'color': faker.color()
}


# Test Definition
@pytest.mark.asyncio
async def test_create(client: AsyncClient):
    """
    Given a name and a color,
    When we execute POST on /sections/,
    Then app shall return status 200 with the data of the new section.
    """
    response = await client.post(
        "/sections/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "name": pytest.section_before.get('name'),
            "color": pytest.section_before.get('color'),
        })
    assert response.status_code == 200

    pytest.section_before['id'] = response.json().get('id')

    pytest.section_after['id'] = response.json().get('id')

    assert response.json() == pytest.section_before


@pytest.mark.asyncio
async def test_get_all_01(client: AsyncClient):
    """
    Given nothing,
    When we execute GET on /sections/ and at least one section exist,
    Then app shall return status 200 with a list of all the sections.
    """
    response = await client.get(
        "/sections/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_default)})
    assert response.status_code == 200
    assert pytest.section_before in response.json()


@pytest.mark.asyncio
async def test_get_one_by_id_01(client: AsyncClient):
    """
    Given an ID,
    When we execute GET on /sections/{ID} and the section exist,
    Then app shall return status 200 with the data of the section found .
    """
    response = await client.get(
        "/sections/{}".format(pytest.section_before.get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_default)})
    assert response.status_code == 200
    assert response.json() == pytest.section_before


@pytest.mark.asyncio
async def test_update_01(client: AsyncClient):
    """
    Given an id with name and a color,
    When we execute PUT on /sections/{ID} and the section exist,
    Then app shall return status 200 with the data of the section and update its data in the DB.
    """
    response = await client.put(
        "/sections/{}".format(pytest.section_after.get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "name": pytest.section_after.get('name'),
            "color": pytest.section_after.get('color'),
        })
    assert response.status_code == 200
    assert response.json() == pytest.section_after


@pytest.mark.asyncio
async def test_get_all_02(client: AsyncClient):
    """
    Given nothing,
    When we execute GET on /sections/,
    Then app shall return status 200 with a list of all the sections.
    """
    response = await client.get(
        "/sections/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_default)})
    assert response.status_code == 200
    assert pytest.section_after in response.json()


@pytest.mark.asyncio
async def test_get_one_by_id_02(client: AsyncClient):
    """
    Given an ID,
    When we execute GET on /sections/{ID},
    Then app shall return status 200 with the data of the section found.
    """
    response = await client.get(
        "/sections/{}".format(pytest.section_after.get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_default)})
    assert response.status_code == 200
    assert response.json() == pytest.section_after


@pytest.mark.asyncio
async def test_delete(client: AsyncClient):
    """
    Given an ID,
    When we execute DELETE on /sections/{ID} and the section exist,
    Then app shall return status 200 with the data of the section and delete the section in the DB.
    """
    response = await client.delete(
        "/sections/{}".format(pytest.section_after.get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)})
    assert response.status_code == 200
    assert response.json() == pytest.section_after


@pytest.mark.asyncio
async def test_get_nothing_by_id_01(client: AsyncClient):
    """
    Given an ID,
    When we execute GET on /sections/{ID} and the section doesn't exist,
    Then app shall return status 404.
    """
    response = await client.get(
        "/sections/{}".format(pytest.section_after.get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_default)})
    assert response.status_code == 404
