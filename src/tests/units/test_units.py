# Import library
import pytest
from httpx import AsyncClient
from faker import Faker


"""
This file contain the tests related to the units routes.
"""


# False Data Setup
faker = Faker()


# Test Variable of unit before the update
pytest.unit_before = {
    'id': None,
    'name': faker.word()
}

# Test Variable of unit after the update
pytest.unit_after = {
    'id': None,
    'name': faker.word()
}


# Test Definition
@pytest.mark.asyncio
async def test_create(client: AsyncClient):
    """
    Given a name,
    When we execute POST on /units/,
    Then app shall return status 200 with the data of the new unit.
    """
    response = await client.post(
        "/units/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "name": pytest.unit_before.get('name'),
        })
    assert response.status_code == 200

    pytest.unit_before['id'] = response.json().get('id')

    pytest.unit_after['id'] = response.json().get('id')

    assert response.json() == pytest.unit_before


@pytest.mark.asyncio
async def test_get_all_01(client: AsyncClient):
    """
    Given nothing,
    When we execute GET on /units/ and at least one unit exist,
    Then app shall return status 200 with a list of all the units.
    """
    response = await client.get(
        "/units/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_default)})
    assert response.status_code == 200
    assert pytest.unit_before in response.json()


@pytest.mark.asyncio
async def test_get_one_by_id_01(client: AsyncClient):
    """
    Given an ID,
    When we execute GET on /units/{ID} and the unit exist,
    Then app shall return status 200 with the data of the unit found .
    """
    response = await client.get(
        "/units/{}".format(pytest.unit_before.get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_default)})
    assert response.status_code == 200
    assert response.json() == pytest.unit_before


@pytest.mark.asyncio
async def test_update_01(client: AsyncClient):
    """
    Given an id with name,
    When we execute PUT on /units/{ID} and the unit exist,
    Then app shall return status 200 with the data of the unit and update its data in the DB.
    """
    response = await client.put(
        "/units/{}".format(pytest.unit_after.get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "name": pytest.unit_after.get('name'),
        })
    assert response.status_code == 200
    assert response.json() == pytest.unit_after


@pytest.mark.asyncio
async def test_get_all_02(client: AsyncClient):
    """
    Given nothing,
    When we execute GET on /units/,
    Then app shall return status 200 with a list of all the units.
    """
    response = await client.get(
        "/units/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_default)})
    assert response.status_code == 200
    assert pytest.unit_after in response.json()


@pytest.mark.asyncio
async def test_get_one_by_id_02(client: AsyncClient):
    """
    Given an ID,
    When we execute GET on /units/{ID},
    Then app shall return status 200 with the data of the unit found.
    """
    response = await client.get(
        "/units/{}".format(pytest.unit_after.get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_default)})
    assert response.status_code == 200
    assert response.json() == pytest.unit_after


@pytest.mark.asyncio
async def test_delete(client: AsyncClient):
    """
    Given an ID,
    When we execute DELETE on /units/{ID} and the unit exist,
    Then app shall return status 200 with the data of the unit and delete the unit in the DB.
    """
    response = await client.delete(
        "/units/{}".format(pytest.unit_after.get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)})
    assert response.status_code == 200
    assert response.json() == pytest.unit_after


@pytest.mark.asyncio
async def test_get_nothing_by_id_01(client: AsyncClient):
    """
    Given an ID,
    When we execute GET on /units/{ID} and the unit doesn't exist,
    Then app shall return status 404.
    """
    response = await client.get(
        "/units/{}".format(pytest.unit_after.get('id')),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_default)})
    assert response.status_code == 404
