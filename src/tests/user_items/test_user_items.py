# Import library
import pytest
from httpx import AsyncClient
from faker import Faker
from random import randint, uniform

"""
This file contain the tests related to the user items routes.
"""

# False Data Setup
faker = Faker()

# Test Variable of user item before the update

pytest.user_item_before = {
    "id": None,
    "quantity": round(uniform(1, 10)),
    "wasted": True,
    "created_at": faker.date(),
    "expire_at": faker.date(),
    "deleted_at": faker.date(),
    "user": {
        "id": 1,  # id du root
        "username": faker.word(),
        "email": faker.word(),
        "updated_at": None,
        "created_at": None,
    },
    "item": {
        "id": None,
        "name": faker.word(),
        "img_path": faker.image_url(),
        "brand": {
            "id": None,
            "name": faker.word(),
        },
        "default_quantity": randint(1, 10),
        "unit": {
            "id": None,
            "name": faker.word(),
        },
        "section": {
            "id": None,
            "name": faker.word(),
        },
        "category": {
            "id": None,
            "name": faker.word(),
        },
        "brand_id": None,
        "unit_id": None,
        "section_id": None,
        "category_id": None,
    },
}

# Test Variable of item after the update
pytest.user_item_after = {
    "id": None,
    "quantity": round(uniform(1, 10)),
    "wasted": False,
    "created_at": faker.date(),
    "expire_at": faker.date(),
    "deleted_at": faker.date(),
    "user": {
        "id": None,
        "username": faker.word(),
        "email": faker.word(),
        "updated_at": None,
        "created_at": None,
    },
    "item": {
        "id": None,
        "name": faker.word(),
        "img_path": faker.image_url(),
        "brand": {
            "id": None,
            "name": faker.word(),
        },
        "default_quantity": randint(1, 10),
        "unit": {
            "id": None,
            "name": faker.word(),
        },
        "section": {
            "id": None,
            "name": faker.word(),
            "color": faker.color(),
        },
        "category": {
            "id": None,
            "name": faker.word(),
        },
        "brand_id": None,
        "unit_id": None,
        "section_id": None,
        "category_id": None,
    },
}


# Assign user
@pytest.mark.asyncio
async def test_get_current_user(client: AsyncClient):
    """
    Given a user token,
    When we try to get our user informations,
    Then app shall return status 200 and our valid user,
    """
    response = await client.get(
        "/users/my",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
    )

    assert response.status_code == 200

    pytest.user_item_before["user"] = response.json()
    pytest.user_item_after["user"] = response.json()


# Test Definition
@pytest.mark.asyncio
async def test_create_brand(client: AsyncClient):
    """
    Given a name,
    When we execute POST on /brands/,
    Then app shall return status 200 with the data of the new brand.
    """
    response = await client.post(
        "/brands/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "name": pytest.user_item_before.get("item").get("brand").get("name"),
        },
    )

    assert response.status_code == 200

    pytest.user_item_before["item"]["brand"] = response.json()
    pytest.user_item_before["item"]["brand_id"] = response.json()["id"]

    pytest.user_item_after["item"]["brand"] = response.json()
    pytest.user_item_after["item"]["brand_id"] = response.json()["id"]

    assert response.json() == pytest.user_item_before.get("item").get("brand")


@pytest.mark.asyncio
async def test_create_unit(client: AsyncClient):
    """
    Given a name,
    When we execute POST on /units/,
    Then app shall return status 200 with the data of the new unit.
    """
    response = await client.post(
        "/units/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "name": pytest.user_item_before.get("item").get("unit").get("name"),
        },
    )
    assert response.status_code == 200

    pytest.user_item_before["item"]["unit"] = response.json()
    pytest.user_item_before["item"]["unit_id"] = response.json()["id"]
    pytest.user_item_after["item"]["unit"] = response.json()
    pytest.user_item_after["item"]["unit_id"] = response.json()["id"]
    assert response.json() == pytest.user_item_before.get("item").get("unit")


@pytest.mark.asyncio
async def test_create_section(client: AsyncClient):
    """
    Given a name,
    When we execute POST on /sections/,
    Then app shall return status 200 with the data of the new section.
    """
    response = await client.post(
        "/sections/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "name": pytest.user_item_before.get("item").get("section").get("name"),
        },
    )
    assert response.status_code == 200

    pytest.user_item_before["item"]["section"] = response.json()
    pytest.user_item_before["item"]["section_id"] = response.json()["id"]
    pytest.user_item_after["item"]["section"] = response.json()
    pytest.user_item_after["item"]["section_id"] = response.json()["id"]

    assert response.json() == pytest.user_item_before.get("item").get("section")


@pytest.mark.asyncio
async def test_create_category(client: AsyncClient):
    """
    Given a name,
    When we execute POST on /categories/,
    Then app shall return status 200 with the data of the new category.
    """
    response = await client.post(
        "/categories/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "name": pytest.user_item_before.get("item").get("category").get("name"),
        },
    )
    assert response.status_code == 200

    pytest.user_item_before["item"]["category"] = response.json()
    pytest.user_item_before["item"]["category_id"] = response.json()["id"]
    pytest.user_item_after["item"]["category"] = response.json()
    pytest.user_item_after["item"]["category_id"] = response.json()["id"]

    assert response.json() == pytest.user_item_before.get("item").get("category")


@pytest.mark.asyncio
async def test_create_item(client: AsyncClient):
    """
    Given a name,
    When we execute POST on /items/,
    Then app shall return status 200 with the data of the new item.
    """
    response = await client.post(
        "/items/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "name": pytest.user_item_before.get("item").get("name"),
            "img_path": pytest.user_item_before.get("item").get("img_path"),
            "brand_id": pytest.user_item_before.get("item").get("brand").get("id"),
            "default_quantity": pytest.user_item_before.get("item").get(
                "default_quantity"
            ),
            "unit_id": pytest.user_item_before.get("item").get("unit").get("id"),
            "section_id": pytest.user_item_before.get("item").get("section").get("id"),
            "category_id": pytest.user_item_before.get("item")
            .get("category")
            .get("id"),
        },
    )
    assert response.status_code == 200

    pytest.user_item_before["item"]["id"] = response.json().get("id")
    pytest.user_item_after["item"]["id"] = response.json().get("id")

    pytest.user_item_before["item"]["name"] = response.json().get("name")
    pytest.user_item_after["item"]["name"] = response.json().get("name")

    pytest.user_item_before["item"]["img_path"] = response.json().get("img_path")
    pytest.user_item_after["item"]["img_path"] = response.json().get("img_path")

    pytest.user_item_before["item"]["default_quantity"] = response.json().get(
        "default_quantity"
    )
    pytest.user_item_after["item"]["default_quantity"] = response.json().get(
        "default_quantity"
    )

    assert response.json() == pytest.user_item_before.get("item")


# Begin user item tests
@pytest.mark.asyncio
async def test_create_user_item(client: AsyncClient):
    """
    Given a name,
    When we execute POST on /user_items/,
    Then app shall return status 200 with the data of the new user_item.
    """
    response = await client.post(
        "/user_items/",
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "quantity": pytest.user_item_before.get("quantity"),
            "expire_at": pytest.user_item_before.get("expire_at"),
            "deleted_at": pytest.user_item_before.get("deleted_at"),
            "wasted": pytest.user_item_before.get("wasted"),
            "user_id": pytest.user_item_before.get("user").get("id"),
            "item_id": pytest.user_item_before.get("item").get("id"),
        },
    )

    assert response.status_code == 200

    pytest.user_item_before["id"] = response.json().get("id")
    pytest.user_item_after["id"] = response.json().get("id")
    pytest.user_item_before["created_at"] = response.json().get("created_at")
    pytest.user_item_after["created_at"] = response.json().get("created_at")

    print(response.json())
    print(pytest.user_item_before)

    assert response.json() == pytest.user_item_before


@pytest.mark.asyncio
async def test_get_one_by_id_01(client: AsyncClient):
    """
    Given an ID,
    When we execute GET on /user_items/{ID} and the unit exist,
    Then app shall return status 200 with the data of the unit found .
    """
    response = await client.get(
        "/user_items/{}".format(pytest.user_item_before.get("id")),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
    )
    assert response.status_code == 200
    assert response.json() == pytest.user_item_before


@pytest.mark.asyncio
async def test_update_01(client: AsyncClient):
    """
    Given an id with name,
    When we execute PUT on /user_items/{ID} and the unit exist,
    Then app shall return status 200 with the data of the unit and update its data in the DB.
    """
    response = await client.put(
        "/user_items/{}".format(pytest.user_item_after.get("id")),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
        json={
            "quantity": pytest.user_item_after.get("quantity"),
            "wasted": pytest.user_item_after.get("wasted"),
            "expire_at": pytest.user_item_after.get("expire_at"),
            "deleted_at": pytest.user_item_after.get("deleted_at"),
            "user_id": pytest.user_item_after.get("user").get("id"),
            "item_id": pytest.user_item_after.get("item").get("id"),
        },
    )
    print(pytest.user_item_after)
    print(response.json())

    assert response.status_code == 200
    assert response.json() == pytest.user_item_after


@pytest.mark.asyncio
async def test_get_one_by_id_02(client: AsyncClient):
    """
    Given an ID,
    When we execute GET on /user_items/{ID},
    Then app shall return status 200 with the data of the unit found.
    """
    response = await client.get(
        "/user_items/{}".format(pytest.user_item_after.get("id")),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
    )
    assert response.status_code == 200
    assert response.json() == pytest.user_item_after


@pytest.mark.asyncio
async def test_delete(client: AsyncClient):
    """
    Given an ID,
    When we execute DELETE on /user_items/{ID} and the unit exist,
    Then app shall return status 200 with the data of the unit and delete the unit in the DB.
    """
    response = await client.delete(
        "/user_items/{}".format(pytest.user_item_after.get("id")),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
    )
    assert response.status_code == 200
    assert response.json() == pytest.user_item_after


@pytest.mark.asyncio
async def test_get_nothing_by_id_01(client: AsyncClient):
    """
    Given an ID,
    When we execute GET on /user_items/{ID} and the unit doesn't exist,
    Then app shall return status 404.
    """
    response = await client.get(
        "/user_items/{}".format(pytest.user_item_after.get("id")),
        headers={"Authorization": "Bearer {}".format(pytest.global_access_token_admin)},
    )
    assert response.status_code == 404
