# Import library
import uvicorn


# If Main Launch It
if __name__ == "__main__":
    uvicorn.run(
        "app.app:app",
        host="0.0.0.0",
        port=8001,
        debug=True,
        reload=True,
        reload_dirs=[
            'app',
            'models',
            'router',
            'tests'
        ],
        lifespan="on"
    )
