FROM python:3.9-slim-buster

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apt-get update

RUN apt-get install -y netcat

COPY ./src/requirements.txt /app/requirements.txt

RUN pip install --no-cache-dir -r /app/requirements.txt

COPY ./src /app

WORKDIR /app

RUN chmod 777 -R *

RUN rm -R migrations

RUN flake8

EXPOSE 80

ENTRYPOINT ["/app/entrypoint.sh"]

CMD ["uvicorn", "app.app:app", "--host", "0.0.0.0", "--port", "80"]
