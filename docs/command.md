# Cellarium : Backend : Command

![Icon](../icon.png)

## Table Of Contents

- [Cellarium : Backend : Command](#cellarium--backend--command)
  - [Table Of Contents](#table-of-contents)
  - [Command](#command)

## Command

    cd src

    # Init Project with Pip
    pip install --no-cache-dir -r /app/requirements.txt

    # Init Project with Pipenv
    pipenv install -r requirements.txt
    pipenv shell

    # Init Project with PDM
    pdm init

    # Init Aerich (Just for create the config file !)
    aerich init -t app.database.TORTOISE_ORM

    # Init Database (Generate schema and generate app migrate location)
    aerich init-db

    # Upgrade database to lastest version
    aerich upgrade

    # Generate database migrate changes file
    aerich migrate

    # Run Unit Test
    pytest

    # Check Synthax
    flake8

    # Start Development
    uvicorn "app.app:app" \
      --host "0.0.0.0" \
      --port 8001 \
      --debug \
      --reload \
      --reload-dir "app" \
      --reload-dir "models" \
      --reload-dir "router" \
      --reload-dir "tests"

    # Start Production
    uvicorn "app.app:app" \
      --host "0.0.0.0" \
      --port 8001
