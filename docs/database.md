# Cellarium : Backend : Database

![Icon](../icon.png)

## Table Of Contents

- [Cellarium : Backend : Database](#cellarium--backend--database)
  - [Table Of Contents](#table-of-contents)
  - [Database](#database)
  - [Database Schema](#database-schema)
  - [Database Idea](#database-idea)

## Database

**Database Name** : cellarium

**Table List** :

- **User** : Users Management
- **Section** : Section Management (Meat, Vegetable, Drink, ...)
- **Category** : Category Management (Beef, Coke, Apple, ...)
- **Unit** : Unit Management (Number, Volume, Weight, ...)
- **Brand** : Brand Management
- **Item** : Item Management
- **User_Item** : Item of User in stock
- **Shopping_Item** : Item in Shopping List
- **Shopping_List** : Shopping List Management

## Database Schema

![Database](./img/database.png)

## Database Idea

![Database Idea 001](./img/database_idea_001.jpeg)
